                            </div><!-- #content -->
                        </div><!-- .row -->
                    </div><!-- .container .main-content-area -->
                </div><!-- #content -->
            <div id="footer-area">
                <div id="footer" class="col-md-12 navbar navbar-default">
                	<div class="col-md-6 text-left">
                		<?php printf(__('Copyleft - %s Compartir es mucho mejor','BetelgeuseTheme'), date('Y')); ?>
                	</div><!-- col-md-6 -->
                	<div class="col-md-6 text-right">
                		<a href="https://gitlab.com/zagur/BetelgeuseTheme" target="_blank">
                            Betelgeuse Theme
                		</a> |
            			<a href="http://www.entrebits.org" target="_blank">
            				<strong>Jesús Camacho</strong>
                        </a>
                	</div><!-- col-md-6 -->
                </div><!-- #footer -->
            </div><!-- #footer-area -->
            <?php nc_cookiesMessage(); ?>
        </div><!-- #page -->
        
        <!-- Scripts -->
        <script src="<?php bloginfo('template_url')?>/js/jquery.min.js"></script>
        <script src="<?php bloginfo('template_url')?>/js/bootstrap.min.js"></script>
        <script src="<?php bloginfo('template_url')?>/js/betelgeuse.js"></script>

        <?php wp_footer(); ?>
    </body>
</html>
