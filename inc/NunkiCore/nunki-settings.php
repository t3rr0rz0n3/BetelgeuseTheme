<?php

/* 			SET OPTIONS TO DISPLAY THE PANEL NUNKI CORE				*/

/* GENERAL SETTINGS */
$ThemeStylesheet = 0;
$Favicon = 1;
$TrackingCode = 1;
$LayoutStyle = 1;
$Ads = 1;
$CookiesNotice = 1;
$BreadCrumb = 1;
$ToTopEffect = 1;
$RandomPosts = 1;
$CollaborateMessage = 0;
$RelatedPosts = 0;

/* SECTIONS */
$SectionsHome = 0;

/* HEADER */
$HeaderBackground = 0;
$HeaderGlyphicons = 1;
$HeaderBrand = 1;

/* SLIDER */
$Slider = 0;

/* TYPOGRAPHY */

/* INFINITE SCROLL */
$InfiniteScroll = 1;

/* SOCIAL OPTIONS */
$SocialOptions = 1;

/* LICENSE */
$SelectLicense = 0;

/* FOOTER OPTIONS */
$FooterOptions = 1;

/* THEME UPDATE */
$ThemeUpdate = 1;

/* OTHERS */
$ChristmassTree = 1;
$CSSExtra = 1;
$JSExtra = 1;

?>
