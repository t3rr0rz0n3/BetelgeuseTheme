<?php

/**
 * Nunki Core
 *
 * @package   Options Framework
 * @author    Jesús Camacho <zagurblog@gmail.com>
 * @license   GPL-3v
 * @link      http://zagur.github.io/nunkicore
 *
 * @wordpress-plugin
 * Plugin Name: Nunki Core
 * Plugin URI:  http://zagur.github.io/nunkicore
 * Description: A framework for building theme options.
 * Version:     0.2.6v
 * Author:      Jesús Camacho
 * Author URI:  http://zagur.github.io
 * License:     GPL-3v
 * License URI: http://www.gnu.org/licenses/gpl-3.0.txt
 * Text Domain: nunkicore, optionsframework
 */

 function NunkiEngineUpdate( $transient ) {
     if ( empty( $transient->checked ) ) {
         return $transient;
     }

     $theme_data = wp_get_theme(wp_get_theme()->template);
     $theme_slug = $theme_data->get_template();
     //Delete '-master' from the end of slug
     $theme_uri_slug = preg_replace('/-master$/', '', $theme_slug);
     $branch = of_get_option('branch');

     $remote_version = '0.0.0';
     $style_css = wp_remote_get("https://gitlab.com/zagur/".$theme_uri_slug."/"."raw"."/".$branch."/style.css")['body'];
     if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( 'Version', '/' ) . ':(.*)$/mi', $style_css, $match ) && $match[1] )
         $remote_version = _cleanup_header_comment( $match[1] );

     if (version_compare($theme_data->version, $remote_version, '<')) {
         $transient->response[$theme_slug] = array(
             'theme'       => $theme_slug,
             'new_version' => $remote_version,
             'url'         => 'https://gitlab.com/zagur/'.$theme_uri_slug,
             'package'     => 'https://gitlab.com/zagur/'.$theme_uri_slug.'/repository/archive.zip?ref='.$branch,
         );
     }
     return $transient;
 }
 add_filter( 'pre_set_site_transient_update_themes', 'NunkiEngineUpdate' );

 /* ######################################################################## */
 /* 				   THEME OPTIONS FUNCTIONS           					*/
 /* ###################################################################### */

 /* #####################
 /* 		GENERAL SETTINGS
 /* ####################################################### */

 /*
 <?php echo of_get_option( 'style', 'style.min.css' ); ?>
 */

 /* ################################ HEADER ###*/
 /*
 <?php echo of_get_option( 'favicon' ); ?>
 */

/* trackingcode */

/* Layout Style */
function nc_sidebarPosition() {
    $position = of_get_option('sidebar_position');
    if ($position == "sidebar_left") {
        $stylePosition = 'style="float:right"';
        return $stylePosition;
    }
}

/* ################################ GOOGLE ADS ###*/



/* ################################ ADVANCED SETTINGS ###*/
/* COOKIES MESSAGE*/
function nc_cookiesMessage() {
    $stateCheckboxCookies = of_get_option('cookies');

    if ($stateCheckboxCookies == 1) {
        if (is_home()) {
            echo '<div class="cookies oculto close-capa cursor-pointer"><span id="cookie" tabindex="0" class="glyphicon glyphicon-cog" data-container="body" data-toggle="popover" data-trigger="focus" data-placement="left" data-content="' . of_get_option('cookieshome', 'NunkiCore') . '"></span></div>';
        } else {
            echo '<div class="cookies oculto close-capa cursor-pointer"><p>' . of_get_option('cookiesall', 'NunkiCore') . '</p></div>';
        }
    }
}

/* BREADCRUMBS */
function nc_breadcrumb() {
    $stateCheckboxBreadCrumb = of_get_option('breadcrumb');

    if ($stateCheckboxBreadCrumb == 1) {
        if (!is_home()) {

            echo '<a href="' . esc_url(home_url( '/' )) . '"><span class="glyphicon glyphicon-home"></span> </a>';
    		echo '<span>' . bloginfo('name') . '</span>';
    		echo ' <span class="fa fa-angle-right"></span> ';
            if (is_single()) {
                the_category('title_li=');
                echo ' <span class="fa fa-angle-right"></span> ';
                the_title();
            }
            if (is_category()) {
                the_category('title_li=');
            }
            if (is_tag()) {
                the_tags('');
            }
            if (is_search()) {
                printf( __( 'Resultados para: %s', 'NunkiCore' ), '<span>' . get_search_query() . '</span>' );
            }
            if (is_page()) {
                the_title();
            }
    	}
    }
}

/* TO TOP EFFECT */
function nc_toTopPage() {
    $stateCheckBoxToTop = of_get_option('totop');

    if ($stateCheckBoxToTop == 1) {
        echo '<a id="backtotop" href="#" onclick="toTopEffect();return false"><span class="glyphicon glyphicon-chevron-up"></span></a>';
    }
}

/* RANDOM POST */
function nc_randomPosts() {
    $stateCheckTrendingArticles = of_get_option('rdmpost');

    if ($stateCheckTrendingArticles == 1) {
        echo '<div class="trending-articles">';
        echo '<ul>';
        echo '<li class="firstlink">Lo más visitado: </li>';
        echo popularPosts();
        echo '</ul>';
        echo '</div>';
    }
}

/* ANNOUNCEMENT TO COLLABORATE */
function nc_collaborate() {
    $stateCheckColaborateAnnouncement = of_get_option('collaborate');

    if ($stateCheckColaborateAnnouncement == 1) {

        echo '<div class="collaborate panel close-panel oculto animated" role="alert">';
        echo '<div class="panel-body">';
        echo '<button type="button" class="close fade" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        echo '<p class="text-muted">' . __('¡Forma parte de nuestro equipo de redactores!', 'AlpheratzTheme') . '</p>';
        echo '<a href="' . of_get_option('collaborateurl') . '" target="_blank"><button type="button" class="btn btn-default btn-collaborate">' . __('Saber más', 'AlpheratzTheme') . '</button></a>';
        echo '</div>';
        echo '</div>';
    }
}

/* RELATED POST */
function nc_relatedPosts() {

    $stateCheckRelatedPosts = of_get_option('relatedposts');

    if ($stateCheckRelatedPosts == 1) {
        $orig_post = $post;
        global $post;
        $categories = get_the_category($post->ID);

        if ($categories) {
            $category_ids = array();

            foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

            $args=array(
                'category__in' => $category_ids,
                'post__not_in' => array($post->ID),
                'posts_per_page'=> 3, // Number of related posts that will be shown.
                'caller_get_posts'=>1
            );

            $my_query = new wp_query( $args );

            if( $my_query->have_posts() ) {

                while( $my_query->have_posts() ) {
                    $my_query->the_post();?>

                    <div class="square col-xs-12 col-sm-4 col-md-4">
                        <a href="<?php the_permalink()?>" rel="bookmark" target="_blank" title="<?php the_title(); ?>">
                            <div class="thumb-related">
                                <div class="thumb-image" style="background-image:url(
                                <?php if (has_post_thumbnail() ) {
                                    the_post_thumbnail_url();
                                } else {
                                    bloginfo('template_directory');
                                    echo '/img/default-thumb.png';
                                } ?>
                                );"></div>
                            </div>
                            <h3><?php the_title(); ?></h3>
                        </a>
                    </div>
                <?
                }
            }
        }
        $post = $orig_post;
        wp_reset_query();
    }
}

/* SHARE CONTENT */
function nc_shareContent() {
    $stateCheckShareContent = of_get_option('sharecontent');

    if ($stateCheckShareContent == 1) {
        get_template_part('inc/NunkiCore/content-parts/content', 'share');
    }
}

/* ###########
/* 		HEADER (necesario?)
/* ####################################################### */
/* CHANGE BACKGROUND HEADER */
function nc_getImageHeader() {
    // Si es index.php
    if (is_home()) { // if is home
        if (of_get_option( 'imgheader' )) { // If is home and upload image
            $url = of_get_option( 'imgheader' );
            return $url;
        } else { // if is home but not upload image
            $url = bloginfo('template_url') . "/img/headerbg.png";
            return $url;
        }
    } else if (is_single()) { // if is single show thumbnail.
        $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
        return $url;
    }
}

/* Glyphicons */
/* <?php echo of_get_option( 'glyphicon', 'glyphicon glyphicon-flash' ); ?> */

/* Título principal de la web: Primera palabra */
/* <?php echo of_get_option('firstword', 'Test'); ?> */

/* Título principal de la web: Segunda palabra */
/* <?php echo of_get_option('secondword'); ?> */

/* ###########
/* 		SLIDER
/* ####################################################### */
function nc_slider() {
    $stateCheckBoxSliderOn = of_get_option('slideon');

    if ($stateCheckBoxSliderOn == 1) {
        get_template_part('inc/NunkiCore/content-parts/content', 'slider');
    }
}

function nc_pauseOnHover() {
    $stateCheckPauseOnHover = of_get_option('pauseonhover');

    if ($stateCheckPauseOnHover == 1) {
        return "hover";
    }
}

/* categoriaslider */

/* numpost */

/* interval */

/* pauseonhover */

/* ###############
/* 		TYPOGRAPHY
/* ####################################################### */


/* ####################
/* 		INVINITE SCROLL
/* ####################################################### */

// LOAD JAVASCRIPT INFINITESCROLL
function nc_InfiniteScrollJS(){
	wp_register_script( 'infinite_scroll',  get_template_directory_uri() . '/inc/NunkiCore/js/jquery.infinitescroll.min.js', array('jquery'),null,true );
	if( ! is_singular() ) {
		wp_enqueue_script('infinite_scroll');
	}
}
add_action('wp_enqueue_scripts', 'nc_InfiniteScrollJS');

/* INFINITE SCROLL */
function nc_InfiniteScroll() {

    $stateCheckboxInfiniteScroll = of_get_option('infinitescroll');

    if ($stateCheckboxInfiniteScroll == 1) {
        if( ! is_singular() ) { ?>
    	<script>
    	var infinite_scroll = {
    		loading: {
    			img: "<?php echo get_template_directory_uri(); ?>/img/loading.gif",
    			msgText: "<?php _e( 'Cargando entradas...', 'AlpheratzTheme' ); ?>",
    			finishedMsg: "<?php _e( 'Has llegado al final!', 'AlpheratzTheme' ); ?>",
                animate : true // if the page will do an animated scroll when new content loads
    		},
    		"nextSelector":"<?php echo of_get_option('nextselector', 'li a.next'); ?>",
    		"navSelector":"<?php echo of_get_option('navselector', '.navigation'); ?>",
    		"itemSelector":"<?php echo of_get_option('itemselector', 'article'); ?>",
    		"contentSelector":"<?php echo of_get_option('contentselector', '#main'); ?>"
    	};
    	jQuery( infinite_scroll.contentSelector ).infinitescroll( infinite_scroll );
    	</script>
    	<?php
    	}
    }
}
add_action( 'wp_footer', 'nc_InfiniteScroll',200 );

/* ###################
/* 		SOCIAL OPTIONS
/* ####################################################### */
/* SOCIAL NETWORK */
function nc_SocialNetworkFooter() {
    $tw = of_get_option( 'social_tw', '#' );
    if (of_get_option('social_tw') != "" ) {
        echo "<li><a target='_blank' class='tw' href=" . "$tw" . "><i class='fa fa-twitter'></i></a></li>";
    }

    $fb = of_get_option( 'social_fb', '#' );
    if (of_get_option('social_fb') != "" ) {
        echo "<li><a target='_blank' class='fb' href=" . "$fb" . "><i class='fa fa-facebook'></i></a></li>";
    }

    $gp = of_get_option( 'social_gp', '#' );
    if (of_get_option('social_gp') != "" ) {
        echo "<li><a target='_blank' class='gp' href=" . "$gp" . "><i class='fa fa-google-plus'></i></a></li>";
    }

    $gh = of_get_option( 'social_gh', '#' );
    if (of_get_option('social_gh') != "" ) {
        echo "<li><a target='_blank' class='gh' href=" . "$gh" . "><i class='fa fa-github-alt'></i></a></li>";
    }
    $tg = of_get_option( 'social_tg', '#' );
    if (of_get_option('social_tg') != "" ) {
        echo "<li><a target='_blank' class='tg' href=" . "$tg" . "><i class='fa fa-paper-plane'></i></a></li>";
    }

    $rss = of_get_option( 'social_rs', '#' );
    if (of_get_option('social_rs') != "" ) {
        echo "<li><a target='_blank' class='rss' href=" . "$rss" . "><i class='fa fa-rss'></i></a></li>";
    }

    $cc = of_get_option( 'social_cc', '#' );
    if (of_get_option('social_cc') != "" ) {
        echo "<li><a target='_blank' class='ccs' href=" . "$cc" . "><i class='fa fa-creative-commons'></i></a></li>";
    }
    //<li><a target="_blank" class="ins" href="https://instagram.com/zagurito/"><span class="socicon">x</span></a></li>
}


/* ############
/* 		LICENSE
/* ####################################################### */
/* SELECT LICENSE */
function nc_license() {
    $multicheck = of_get_option( 'license', 'none' );

    if ( is_array( $multicheck ) ) {
        foreach ( $multicheck as $key => $value ) {
            // If you need the option's name rather than the key you can get that
            $name = $test_array_jr[$key];
            // Prints out each of the values
            if ($value == 1) {
                echo '<span class="cc ' . $key . '"></span>';
            }
        }
    }
}

/* ###################
/* 		FOOTER OPTIONS
/* ####################################################### */

/* copyright */

/* ##################
/* 		THEME UPDATES
/* ####################################################### */

/* enableupdates */

/* branch */

/* ###########
/* 		OTHERS
/* ####################################################### */
/* CHRISTMAS */
function nc_christmasTree() {
    $stateCheckboxChristmas = of_get_option('christmas');
    $imgTree =  get_template_directory_uri() . '/inc/NunkiCore/images/christmas-tree-icon.png'; // CAMBIAR ESTA RUTA

    if ($stateCheckboxChristmas == 1) {
        echo '<div class="christmas"><img title="Árbol de navidad" alt="Árbol de navidad" src="' . esc_url($imgTree) . '"></div>';
    }
}

/* cssextra */

/* jsextra */















/* Fondo del Tema */
function nc_changeBkgImgOrColor() {
    $background = of_get_option('background');
    if ( $background ) {
        if ( $background['image'] ) {
            $bgc = ' style="background:url(' . $background['image'] . ')"';
            return $bgc;
        } else if ( $background['color'] ) {
            $bgc = ' style="background:' . $background['color'] . '"';
            return $bgc;
        } else {
            return null;
        }
    }
}

/*
function nc_excludeCategoriesHome($query) {
    if ( $query->is_home ) {
        $excludecategories = of_get_option('excludecategory');
        $query->set( 'cat', $excludecategories );
    }
    return $query;
}
add_filter( 'pre_get_posts', 'nc_excludeCategoriesHome ' );
*/


/* ------------------------------
FUNCTIONS NUNKI CORE --------------------*/
// Para que <script> y <ins> funcionen en NUNKI CORE
add_action('admin_init','optionscheck_change_santiziation', 100);

function optionscheck_change_santiziation() {
    remove_filter( 'of_sanitize_textarea', 'of_sanitize_textarea' );
    add_filter( 'of_sanitize_textarea', 'custom_sanitize_textarea' );
}

function custom_sanitize_textarea($input) {
    global $allowedposttags;
    $custom_allowedtags["ins"] = array(
        "class" => array(),
        "style" => array(),
        "data-ad-client" => array(),
        "data-ad-slot" => array()
      );
      $custom_allowedtags["script"] = array(
          "src" => array(),
          "type" => array(),
      );

      $custom_allowedtags = array_merge($custom_allowedtags, $allowedposttags);
      $output = wp_kses( $input, $custom_allowedtags);
      return $output;
}

?>
