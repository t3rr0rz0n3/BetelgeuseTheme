<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="keywords" content="Software Libre, Stallman, Linux, GNU, GNU/Linux">
        <meta name="author" content="Jesús Camacho">
        <meta name="description" content="<?php bloginfo('description'); ?>">
        <title><?php bloginfo('name'); ?> <?php if ( is_single() ) { } ?> <?php wp_title(); ?></title>

        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

        <!-- RSS -->
        <link rel="alternate" type="aplication/rss+xml" title="<?php echo of_get_option('firstword', 'Betelgeuse'); ?>" href="<?php echo esc_url( home_url() ); ?>/feed" />

        <!-- FAVICON -->
        <link rel="shortcut icon" href="<?php echo of_get_option( 'favicon' ); ?>" />
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <!-- Styles -->
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/normalize.css" media="screen" charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/css/bootstrap.min.css" media="screen" charset="utf-8">
        <link rel="stylesheet" href="<?php bloginfo('template_url')?>/style.min.css" media="screen" charset="utf-8">

        <!-- Google Fonts -->
        <link href='https://fonts.googleapis.com/css?family=Voltaire' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>

        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>
        <div id="page">
            <header>
                <nav class="navbar navbar-default navbar-fixed-top">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                        <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <span><?php echo of_get_option('firstword', 'Betelgeuse'); ?></span>
                            <span><?php echo of_get_option('secondword', 'Theme'); ?></span>
                            <span class="glyphicon <?php echo of_get_option( 'glyphicon', 'glyphicon glyphicon-globe' ); ?>"></span>
                        </a>
                    </div>

                    <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse" role="navigation">

                            <?php wp_nav_menu( array(
                                'menu'           => 'primary',
                                'theme_location' => 'primary',
                                'depth'          => 2,
                                'container'      => false,
                                'menu_class'     => 'nav navbar-nav',
                                'walker'         => new wp_bootstrap_navwalker(),
                                'fallback_cb'    => 'wp_bootstrap_navwalker::fallback'
                                )
                            ); ?>

                            <ul class="nav navbar-nav navbar-right">
                                <!--<li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Redes Sociales <span class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="http://planetlibre.es/feed/" target="_blank">RSS<i class="fa fa-rss"></i></a></li>
                                        <li><a href="https://www.facebook.com/pages/PlanetLibre/1472669096284457" target="_blank">Facebook<i class="fa fa-facebook"></i></a></li>
                                        <li><a href="https://twitter.com/planetlibrees" target="_blank">Twitter<i class="fa fa-twitter"></i></a></li>
                                        <li class="disabled"><a href="#" target="_blank"><i clas="fa fa-google-plus"></i>Googles Plus</a></li>
                                    </ul>
                                </li>-->
                                <li class="search-box">
                                    <?php get_search_form(); ?>
                                </li>
                            </ul>

                            <?php wp_nav_menu( array(
                                'menu'           => 'social',
                                'theme_location' => 'social',
                                'depth'          => 2,
                                'container'      => false,
                                'menu_class'     => 'nav navbar-nav navbar-right',
                                'walker'         => new wp_bootstrap_navwalker(),
                                'fallback_cb'    => 'wp_bootstrap_navwalker::fallback'
                                )
                            ); ?>




                    </div>
                </nav>
            </header>
            <div id="pseudomenu"></div><!-- #pseudomenu -->
            <div id="content">

                <div class="container-fluid main-content-area">
                    <div class="row">
                        <div id="content" class="col-md-12 col-sm-12">
                        <!-- continua con #primary y #secondary -->
