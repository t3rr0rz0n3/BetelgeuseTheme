<article <?php post_class();?>>
    <div class="date"><?php the_date('l, d \d\e F Y'); ?></div>
    <?php
        if ( has_post_thumbnail() ) {
            the_post_thumbnail('large', array( 'class' => 'img-responsive' ));
        }
    ?>
    <div class="content-blog">
        <div class="header-blog">
            <h1><?php the_title(); ?></h1>
        </div>
        <div class="text-blog">
            <?php the_content(); ?>
        </div>
    </div><!-- .content-blog -->
</article>
