<div id="sidebar-menu" class="">
    <div class="row">
        <div class="col-md-4 text-center">
            <img class="opennavbar img-responsive img-circle" src="<?php bloginfo('template_url')?>/img/logoPL.png" alt="Haz clic en la imagen" title="Haz clic en la imagen" />
        </div>
        <div class="col-md-8">
            <h2 class="pl text-center">PlanetLibre <span class="glyphicon glyphicon-globe"></span></h2>
            <h3 class="subpl text-center"><?php _e('un projecto de', 'BetelgeuseTheme') ?></h3>
            <h4 class="plautor text-right"><a href="http://zagur.github.io/" target="_blank">Jesús Camacho</a></h4>
        </div>
    </div>

    <div class="blogs-info">
        <div class="col-md-12">
            <h2 class="title-sidebar"><?php _e('Estadisticas', 'BetelgeuseTheme'); ?></h2>
            <ul class="list-group">
                <li class="list-group-item">
                    <span class="badge"><?php echo totalCategories(); ?></span>
                    <?php _e('Número de blogs', 'BetelgeuseTheme'); ?>
                </li>
                <li class="list-group-item">
                    <span class="badge"><?php echo totalPost(); ?></span>
                    <?php _e('Número de entradas', 'BetelgeuseTheme'); ?>
                </li>
            </ul>
            <h2 class="title-sidebar"><?php _e('Top 5 de blogs', 'BetelgeuseTheme'); ?></h2>
            <ul class="list-group">
                <?php topFive(); ?>
            </ul>
        </div>
    </div>
</div>


<!--
<div class="widgets-tests">
    <?php //if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('SideBar') ) : endif; ?>
</div>
-->
