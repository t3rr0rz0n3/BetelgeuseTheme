<article  <?php post_class('par impar');?>>
    <div class="date"><?php the_date('l, d \d\e F Y'); ?></div>
    <div class="post-row row">
        <div class="col-md-3 col-sm-3 col-xs-6">
            <h2 class="blogname">
                <?php
                    $category = get_the_category();
                    if ($category) {
                        echo '<a target="_blank" href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "Ver todas las entradas de %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
                    }
                ?>
            </h2>
        </div><!-- .col-md-3 -->
        <div class="col-md-8 col-sm-8 col-xs-12">
            <h2 class="blogtitle">
                <a target="_blank" href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
            </h2>
        </div><!-- .col-md-9 -->
        <div class="col-md-1 col-sm-1 col-xs-2 col-xs-offset-10 col-md-offset-0">
            <a class="share" data-toggle="modal" data-target="#myModal-<?php the_ID(); ?>">
                <span class="glyphicon glyphicon-share"></span>
            </a>
        </div>
    </div>

    <div class="modal fade" id="myModal-<?php the_ID(); ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-focus-on="input:first">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title text-center"><?php _e('Compartir contenido', 'BetelgeuseTheme') ?></h4>
                </div>
                <div class="modal-body">
                    <h4 class="text-center"><?php _e('Comparte el contenido con la red social que prefieras!','BetelgeuseTheme') ?></h4>
                    <p><?php _e('Vas a compartir el siguiente enlace: ', 'BetelgeuseTheme') ?><?php the_title(); ?></p>
                    <!-- EMAIL -->
                    <div class="share-box email-box col-md-6">
                        <a data-toggle="tooltip" data-placement="top" href="mailto:?subject=<?php the_title(); ?>&body=<?php the_title(); ?>:<?php the_permalink(); ?>" class="share-box-link email" target="_blank" title="Enviar por correo">
                            E-mail
                        </a>
                    </div>
                    <!-- FACEBOOK -->
                    <div class="share-box fb-box col-md-6">
                        <a class="share-box-link" data-toggle="tooltip" data-placement="top" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>&p=http://www.portallinux.es/wp-content/themes/AlpheratzTheme/img/DualBootWin10.png" title="Compartir en Facebook" target="_blank">
                            Facebook
                        </a>
                    </div>
                    <!-- TWITTER -->
                    <div class="share-box tw-box col-md-6">
                        <a class="share-box-link" data-toggle="tooltip" data-placement="top" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>&text=<?php the_title(); ?>&via=zagurito" target="_blank" title="Compartir con Twitter" alt="<?php the_title(); ?>">
                            Twitter
                        </a>
                    </div>
                    <!-- G+ -->
                    <div class="share-box gp-box col-md-6">
                        <a class="share-box-link" data-toggle="tooltip" data-placement="top" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank" title="Compartir con Google+">
                            Google+
                        </a>
                    </div>
                    <!-- REDDIT -->
                    <div class="share-box rd-box col-md-6">
                        <a class="share-box-link" data-toggle="tooltip" data-placement="top" href="http://www.reddit.com/submit?url=<?php the_permalink(); ?>&title=<?php the_title(); ?>" target="_blank" title="Compartir con Reddit">
                            Reddit
                        </a>
                    </div>
                    <!-- GNU SOCIAL -->
                    <div class="share-box gs-box col-md-6 gs-share">
                        <a class="share-box-link" data-toggle="modal" data-target="#gs-social-<?php the_ID(); ?>">
                            GNU Social
                        </a>
                    </div>
                    <!-- DIASPORA -->
                    <div class="share-box dp-box col-md-6">
                        <a class="share-box-link disabled" data-toggle="modal" data-target="#dp-social-<?php the_ID(); ?>">
                            Diaspora*
                        </a>
                    </div>
                    <!-- NAME -->
                    <div class="share-box box col-md-6">
                        <a class="share-box-link disabled" href="#">

                        </a>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Cerrar', 'BetelgeuseTheme') ?></button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <!-- SECOND MODAL GS -->
    <div id="gs-social-<?php the_ID(); ?>" class="modal fade" role="dialog" tabindex="-1" style="z-index: 1600;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Compartir contenido en GNU Social', 'BetelgeuseTheme') ?></h4>
                </div><!-- .modal-header -->
                <div class="modal-body">
                    <form id="form-gs-share-<?php the_ID(); ?>" method="post">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label"><?php _e('URL Nodo: ', 'BetelgeuseTheme') ?></label>
                            <input placeholder="https://" type="text" class="input-form form-control" id="gs-node-<?php the_ID(); ?>">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label"><?php _e('Mensaje:', 'BetelgeuseTheme') ?></label>
                            <textarea placeholder="<?php _e('¡Coméntalo en tu nodo de GNU Social!', 'BetelgeuseTheme') ?>" class="input-form form-control" id="gs-msg-<?php the_ID(); ?>"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label"><?php _e('URL que vas a compartir:', 'BetelgeuseTheme') ?></label>
                            <input type="text" class="input-form form-control" id="gs-url-<?php the_ID(); ?>" value="<?php the_permalink(); ?>" readonly>
                        </div>
                    </form>
                </div><!-- .modal-body -->
                <div class="modal-footer">
                    <button class="btn btn-success" href="#" role="button" onclick="gsShare(<?php the_ID(); ?>);"><?php _e('Enviar', 'BetelgeuseTheme') ?></button>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Cerrar', 'BetelgeuseTheme') ?></button>
                </div><!-- .modal-footer -->
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- .gs-social  -->

    <!-- SECOND MODAL DIASPORA -->
    <div id="dp-social-<?php the_ID(); ?>" class="modal fade" role="dialog" tabindex="-1" style="z-index: 1600;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><?php _e('Compartir contenido en Diaspora*','BetelgeuseTheme') ?></h4>
                </div><!-- .modal-header -->
                <div class="modal-body">
                    <form id="form-dp-share-<?php the_ID(); ?>" method="post">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label"><?php _e('URL Nodo:','BetelgeuseTheme') ?></label>
                            <input placeholder="https://" type="text" class="input-form form-control" id="dp-node-<?php the_ID(); ?>">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label"><?php _e('Mensaje:','BetelgeuseTheme') ?></label>
                            <textarea placeholder="<?php _e('¡Coméntalo en tu nodo de Diaspora*!','BetelgeuseTheme') ?>" class="input-form form-control" id="dp-msg-<?php the_ID(); ?>"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label"><?php _e('URL que vas a compartir:','BetelgeuseTheme') ?></label>
                            <input type="text" class="input-form form-control" id="dp-url-<?php the_ID(); ?>" value="<?php the_permalink(); ?>" readonly>
                        </div>
                    </form>
                </div><!-- .modal-body -->
                <div class="modal-footer">
                    <a class="btn btn-default" href="#" role="button" onclick="dpShare(<?php the_ID(); ?>);"><?php _e('Enviar', 'BetelgeuseTheme') ?></a>
                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php _e('Cerrar', 'BetelgeuseTheme') ?></button>
                </div><!-- .modal-footer -->
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- .dp-social  -->
</article>
