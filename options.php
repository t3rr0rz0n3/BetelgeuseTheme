

<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 */
function optionsframework_option_name() {
	// Change this to use your theme slug
	return 'options-framework-theme';
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 * If you are making your theme translatable, you should replace 'NunkiCore'
 * with the actual text domain for your theme.  Read more:
 * http://codex.wordpress.org/Function_Reference/load_theme_textdomain
 */

function optionsframework_options() {

	// Styles Theme
	$style = array(
		'' => __( 'None', 'NunkiCore' ),
		'inc/NunkiCore/css/styles/style_red.css' => __( 'Rojo', 'NunkiCore'),
		'inc/NunkiCore/css/styles/style_green.css' => __( 'Verde', 'NunkiCore'),
		'inc/NunkiCore/css/styles/style_yellow.css' => __( 'Amarillo', 'NunkiCore'),
		'inc/NunkiCore/css/styles/style_blue.css' => __( 'Azul', 'NunkiCore'),
		'inc/NunkiCore/css/styles/style_purple.css' => __( 'Lila', 'NunkiCore'),
		'inc/NunkiCore/css/styles/style_aqua.css' => __( 'Agua', 'NunkiCore'),
		'inc/NunkiCore/css/styles/style_orange.css' => __( 'Naranja', 'NunkiCore'),
		'inc/NunkiCore/css/styles/style_mint.css' => __( 'Menta', 'NunkiCore'),
		'inc/NunkiCore/css/styles/style_mediumgrey.css' => __( 'Gris suave', 'NunkiCore'),
		'inc/NunkiCore/css/styles/style_darkgrey.css' => __( 'Gris fuerte', 'NunkiCore')
	);

	// Glyphicons from Bootstrap
	$glyphicons = array(
		'glyphicon-adjust' => __( 'glyphicon glyphicon-adjust', 'NunkiCore' ),
		'glyphicon-alert' => __( 'glyphicon glyphicon-alert', 'NunkiCore' ),
		'glyphicon-align-center' => __( 'glyphicon glyphicon-align-center', 'NunkiCore' ),
		'glyphicon-align-justify' => __( 'glyphicon glyphicon-align-justify', 'NunkiCore' ),
		'glyphicon-align-left' => __( 'glyphicon glyphicon-align-left', 'NunkiCore' ),
		'glyphicon-align-right' => __( 'glyphicon glyphicon-align-right', 'NunkiCore' ),
		'glyphicon-apple' => __( 'glyphicon glyphicon-apple', 'NunkiCore' ),
		'glyphicon-arrow-down' => __( 'glyphicon glyphicon-arrow-down', 'NunkiCore' ),
		'glyphicon-arrow-left' => __( 'glyphicon glyphicon-arrow-left', 'NunkiCore' ),
		'glyphicon-arrow-right' => __( 'glyphicon glyphicon-arrow-right', 'NunkiCore' ),
		'glyphicon-arrow-up' => __( 'glyphicon glyphicon-arrow-up', 'NunkiCore' ),
		'glyphicon-asterisk' => __( 'glyphicon glyphicon-asterisk', 'NunkiCore' ),
		'glyphicon-baby-formula' => __( 'glyphicon glyphicon-baby-formula', 'NunkiCore' ),
		'glyphicon-backward' => __( 'glyphicon glyphicon-backward', 'NunkiCore' ),
		'glyphicon-ban-circle' => __( 'glyphicon glyphicon-ban-circle', 'NunkiCore' ),
		'glyphicon-barcode' => __( 'glyphicon glyphicon-barcode', 'NunkiCore' ),
		'glyphicon-bed' => __( 'glyphicon glyphicon-bed', 'NunkiCore' ),
		'glyphicon-bell' => __( 'glyphicon glyphicon-bell', 'NunkiCore' ),
		'glyphicon-bishop' => __( 'glyphicon glyphicon-bishop', 'NunkiCore' ),
		'glyphicon-bitcoin' => __( 'glyphicon glyphicon-bitcoin', 'NunkiCore' ),
		'glyphicon-blackboard' => __( 'glyphicon glyphicon-blackboard', 'NunkiCore' ),
		'glyphicon-bold' => __( 'glyphicon glyphicon-bold', 'NunkiCore' ),
		'glyphicon-book' => __( 'glyphicon glyphicon-book', 'NunkiCore' ),
		'glyphicon-bookmark' => __( 'glyphicon glyphicon-bookmark', 'NunkiCore' ),
		'glyphicon-briefcase' => __( 'glyphicon glyphicon-briefcase', 'NunkiCore' ),
		'glyphicon-btc' => __( 'glyphicon glyphicon-btc', 'NunkiCore' ),
		'glyphicon-bullhorn' => __( 'glyphicon glyphicon-bullhorn', 'NunkiCore' ),
		'glyphicon-calendar' => __( 'glyphicon glyphicon-calendar', 'NunkiCore' ),
		'glyphicon-camera' => __( 'glyphicon glyphicon-camera', 'NunkiCore' ),
		'glyphicon-cd' => __( 'glyphicon glyphicon-cd', 'NunkiCore' ),
		'glyphicon-certificate' => __( 'glyphicon glyphicon-certificate', 'NunkiCore' ),
		'glyphicon-check' => __( 'glyphicon glyphicon-check', 'NunkiCore' ),
		'glyphicon-chevron-down' => __( 'glyphicon glyphicon-chevron-down', 'NunkiCore' ),
		'glyphicon-chevron-left' => __( 'glyphicon glyphicon-chevron-left', 'NunkiCore' ),
		'glyphicon-chevron-right' => __( 'glyphicon glyphicon-chevron-right', 'NunkiCore' ),
		'glyphicon-chevron-up' => __( 'glyphicon glyphicon-chevron-up', 'NunkiCore' ),
		'glyphicon-circle-arrow-down' => __( 'glyphicon glyphicon-circle-arrow-down', 'NunkiCore' ),
		'glyphicon-circle-arrow-left' => __( 'glyphicon glyphicon-circle-arrow-left', 'NunkiCore' ),
		'glyphicon-circle-arrow-right' => __( 'glyphicon glyphicon-circle-arrow-right', 'NunkiCore' ),
		'glyphicon-circle-arrow-up' => __( 'glyphicon glyphicon-circle-arrow-up', 'NunkiCore' ),
		'glyphicon-cloud' => __( 'glyphicon glyphicon-cloud', 'NunkiCore' ),
		'glyphicon-cloud-download' => __( 'glyphicon glyphicon-cloud-download', 'NunkiCore' ),
		'glyphicon-cloud-upload' => __( 'glyphicon glyphicon-cloud-upload', 'NunkiCore' ),
		'glyphicon-cog' => __( 'glyphicon glyphicon-cog', 'NunkiCore' ),
		'glyphicon-collapse-down' => __( 'glyphicon glyphicon-collapse-down', 'NunkiCore' ),
		'glyphicon-collapse-up' => __( 'glyphicon glyphicon-collapse-up', 'NunkiCore' ),
		'glyphicon-comment' => __( 'glyphicon glyphicon-comment', 'NunkiCore' ),
		'glyphicon-compressed' => __( 'glyphicon glyphicon-compressed', 'NunkiCore' ),
		'glyphicon-console' => __( 'glyphicon glyphicon-console', 'NunkiCore' ),
		'glyphicon-copy' => __( 'glyphicon glyphicon-copy', 'NunkiCore' ),
		'glyphicon-copyright-mark' => __( 'glyphicon glyphicon-copyright-mark', 'NunkiCore' ),
		'glyphicon-credit-card' => __( 'glyphicon glyphicon-credit-card', 'NunkiCore' ),
		'glyphicon-cutlery' => __( 'glyphicon glyphicon-cutlery', 'NunkiCore' ),
		'glyphicon-dashboard' => __( 'glyphicon glyphicon-dashboard', 'NunkiCore' ),
		'glyphicon-download' => __( 'glyphicon glyphicon-download', 'NunkiCore' ),
		'glyphicon-download-alt' => __( 'glyphicon glyphicon-download-alt', 'NunkiCore' ),
		'glyphicon-duplicate' => __( 'glyphicon glyphicon-duplicate', 'NunkiCore' ),
		'glyphicon-earphone' => __( 'glyphicon glyphicon-earphone', 'NunkiCore' ),
		'glyphicon-edit' => __( 'glyphicon glyphicon-edit', 'NunkiCore' ),
		'glyphicon-education' => __( 'glyphicon glyphicon-education', 'NunkiCore' ),
		'glyphicon-eject' => __( 'glyphicon glyphicon-eject', 'NunkiCore' ),
		'glyphicon-envelope' => __( 'glyphicon glyphicon-envelope', 'NunkiCore' ),
		'glyphicon-equalizer' => __( 'glyphicon glyphicon-equalizer', 'NunkiCore' ),
		'glyphicon-erase' => __( 'glyphicon glyphicon-erase', 'NunkiCore' ),
		'glyphicon-eur' => __( 'glyphicon glyphicon-eur', 'NunkiCore' ),
		'glyphicon-euro' => __( 'glyphicon glyphicon-euro', 'NunkiCore' ),
		'glyphicon-exclamation-sign' => __( 'glyphicon glyphicon-exclamation-sign', 'NunkiCore' ),
		'glyphicon-expand' => __( 'glyphicon glyphicon-expand', 'NunkiCore' ),
		'glyphicon-export' => __( 'glyphicon glyphicon-export', 'NunkiCore' ),
		'glyphicon-eye-close' => __( 'glyphicon glyphicon-eye-close', 'NunkiCore' ),
		'glyphicon-eye-open' => __( 'glyphicon glyphicon-eye-open', 'NunkiCore' ),
		'glyphicon-facetime-video' => __( 'glyphicon glyphicon-facetime-video', 'NunkiCore' ),
		'glyphicon-fast-backward' => __( 'glyphicon glyphicon-fast-backward', 'NunkiCore' ),
		'glyphicon-fast-forward' => __( 'glyphicon glyphicon-fast-forward', 'NunkiCore' ),
		'glyphicon-file' => __( 'glyphicon glyphicon-file', 'NunkiCore' ),
		'glyphicon-film' => __( 'glyphicon glyphicon-film', 'NunkiCore' ),
		'glyphicon-filter' => __( 'glyphicon glyphicon-filter', 'NunkiCore' ),
		'glyphicon-fire' => __( 'glyphicon glyphicon-fire', 'NunkiCore' ),
		'glyphicon-flag' => __( 'glyphicon glyphicon-flag', 'NunkiCore' ),
		'glyphicon glyphicon-flash' => __( 'glyphicon glyphicon-flash', 'NunkiCore' ),
		'glyphicon-floppy-disk' => __( 'glyphicon glyphicon-floppy-disk', 'NunkiCore' ),
		'glyphicon-floppy-open' => __( 'glyphicon glyphicon-floppy-open', 'NunkiCore' ),
		'glyphicon-floppy-remove' => __( 'glyphicon glyphicon-floppy-remove', 'NunkiCore' ),
		'glyphicon-floppy-save' => __( 'glyphicon glyphicon-floppy-save', 'NunkiCore' ),
		'glyphicon-floppy-saved' => __( 'glyphicon glyphicon-floppy-saved', 'NunkiCore' ),
		'glyphicon-folder-close' => __( 'glyphicon glyphicon-folder-close', 'NunkiCore' ),
		'glyphicon-folder-open' => __( 'glyphicon glyphicon-folder-open', 'NunkiCore' ),
		'glyphicon-font' => __( 'glyphicon glyphicon-font', 'NunkiCore' ),
		'glyphicon-forward' => __( 'glyphicon glyphicon-forward', 'NunkiCore' ),
		'glyphicon-fullscreen' => __( 'glyphicon glyphicon-fullscreen', 'NunkiCore' ),
		'glyphicon-gbp' => __( 'glyphicon glyphicon-gbp', 'NunkiCore' ),
		'glyphicon-gift' => __( 'glyphicon glyphicon-gift', 'NunkiCore' ),
		'glyphicon-glass' => __( 'glyphicon glyphicon-glass', 'NunkiCore' ),
		'glyphicon-globe' => __( 'glyphicon glyphicon-globe', 'NunkiCore' ),
		'glyphicon-grain' => __( 'glyphicon glyphicon-grain', 'NunkiCore' ),
		'glyphicon-hand-down' => __( 'glyphicon glyphicon-hand-down', 'NunkiCore' ),
		'glyphicon-hand-left' => __( 'glyphicon glyphicon-hand-left', 'NunkiCore' ),
		'glyphicon-hand-right' => __( 'glyphicon glyphicon-hand-right', 'NunkiCore' ),
		'glyphicon-hand-up' => __( 'glyphicon glyphicon-hand-up', 'NunkiCore' ),
		'glyphicon-hdd' => __( 'glyphicon glyphicon-hdd', 'NunkiCore' ),
		'glyphicon-hd-video' => __( 'glyphicon glyphicon-hd-video', 'NunkiCore' ),
		'glyphicon-header' => __( 'glyphicon glyphicon-header', 'NunkiCore' ),
		'glyphicon-headphones' => __( 'glyphicon glyphicon-headphones', 'NunkiCore' ),
		'glyphicon-heart' => __( 'glyphicon glyphicon-heart', 'NunkiCore' ),
		'glyphicon-heart-empty' => __( 'glyphicon glyphicon-heart-empty', 'NunkiCore' ),
		'glyphicon-home' => __( 'glyphicon glyphicon-home', 'NunkiCore' ),
		'glyphicon-hourglass' => __( 'glyphicon glyphicon-hourglass', 'NunkiCore' ),
		'glyphicon-ice-lolly' => __( 'glyphicon glyphicon-ice-lolly', 'NunkiCore' ),
		'glyphicon-ice-lolly-tasted' => __( 'glyphicon glyphicon-ice-lolly-tasted', 'NunkiCore' ),
		'glyphicon-import' => __( 'glyphicon glyphicon-import', 'NunkiCore' ),
		'glyphicon-inbox' => __( 'glyphicon glyphicon-inbox', 'NunkiCore' ),
		'glyphicon-indent-left' => __( 'glyphicon glyphicon-indent-left', 'NunkiCore' ),
		'glyphicon-indent-right' => __( 'glyphicon glyphicon-indent-right', 'NunkiCore' ),
		'glyphicon-info-sign' => __( 'glyphicon glyphicon-info-sign', 'NunkiCore' ),
		'glyphicon-italic' => __( 'glyphicon glyphicon-italic', 'NunkiCore' ),
		'glyphicon-jpy' => __( 'glyphicon glyphicon-jpy', 'NunkiCore' ),
		'glyphicon-king' => __( 'glyphicon glyphicon-king', 'NunkiCore' ),
		'glyphicon-knight' => __( 'glyphicon glyphicon-knight', 'NunkiCore' ),
		'glyphicon-lamp' => __( 'glyphicon glyphicon-lamp', 'NunkiCore' ),
		'glyphicon-leaf' => __( 'glyphicon glyphicon-leaf', 'NunkiCore' ),
		'glyphicon-level-up' => __( 'glyphicon glyphicon-level-up', 'NunkiCore' ),
		'glyphicon-link' => __( 'glyphicon glyphicon-link', 'NunkiCore' ),
		'glyphicon-list' => __( 'glyphicon glyphicon-list', 'NunkiCore' ),
		'glyphicon-list-alt' => __( 'glyphicon glyphicon-list-alt', 'NunkiCore' ),
		'glyphicon-lock' => __( 'glyphicon glyphicon-lock', 'NunkiCore' ),
		'glyphicon-log-in' => __( 'glyphicon glyphicon-log-in', 'NunkiCore' ),
		'glyphicon-log-out' => __( 'glyphicon glyphicon-log-out', 'NunkiCore' ),
		'glyphicon-magnet' => __( 'glyphicon glyphicon-magnet', 'NunkiCore' ),
		'glyphicon-map-marker' => __( 'glyphicon glyphicon-map-marker', 'NunkiCore' ),
		'glyphicon-menu-down' => __( 'glyphicon glyphicon-menu-down', 'NunkiCore' ),
		'glyphicon-menu-hamburger' => __( 'glyphicon glyphicon-menu-hamburger', 'NunkiCore' ),
		'glyphicon-menu-left' => __( 'glyphicon glyphicon-menu-left', 'NunkiCore' ),
		'glyphicon-menu-right' => __( 'glyphicon glyphicon-menu-right', 'NunkiCore' ),
		'glyphicon-menu-up' => __( 'glyphicon glyphicon-menu-up', 'NunkiCore' ),
		'glyphicon-minus' => __( 'glyphicon glyphicon-minus', 'NunkiCore' ),
		'glyphicon-minus-sign' => __( 'glyphicon glyphicon-minus-sign', 'NunkiCore' ),
		'glyphicon-modal-window' => __( 'glyphicon glyphicon-modal-window', 'NunkiCore' ),
		'glyphicon-move' => __( 'glyphicon glyphicon-move', 'NunkiCore' ),
		'glyphicon-music' => __( 'glyphicon glyphicon-music', 'NunkiCore' ),
		'glyphicon-new-window' => __( 'glyphicon glyphicon-new-window', 'NunkiCore' ),
		'glyphicon-object-align-bottom' => __( 'glyphicon glyphicon-object-align-bottom', 'NunkiCore' ),
		'glyphicon-object-align-horizontal' => __( 'glyphicon glyphicon-object-align-horizontal', 'NunkiCore' ),
		'glyphicon-object-align-left' => __( 'glyphicon glyphicon-object-align-left', 'NunkiCore' ),
		'glyphicon-object-align-right' => __( 'glyphicon glyphicon-object-align-right', 'NunkiCore' ),
		'glyphicon-object-align-top' => __( 'glyphicon glyphicon-object-align-top', 'NunkiCore' ),
		'glyphicon-object-align-vertical' => __( 'glyphicon glyphicon-object-align-vertical', 'NunkiCore' ),
		'glyphicon-off' => __( 'glyphicon glyphicon-off', 'NunkiCore' ),
		'glyphicon-oil' => __( 'glyphicon glyphicon-oil', 'NunkiCore' ),
		'glyphicon-ok' => __( 'glyphicon glyphicon-ok', 'NunkiCore' ),
		'glyphicon-ok-circle' => __( 'glyphicon glyphicon-ok-circle', 'NunkiCore' ),
		'glyphicon-ok-sign' => __( 'glyphicon glyphicon-ok-sign', 'NunkiCore' ),
		'glyphicon-open' => __( 'glyphicon glyphicon-open', 'NunkiCore' ),
		'glyphicon-open-file' => __( 'glyphicon glyphicon-open-file', 'NunkiCore' ),
		'glyphicon-option-horizontal' => __( 'glyphicon glyphicon-option-horizontal', 'NunkiCore' ),
		'glyphicon-option-vertical' => __( 'glyphicon glyphicon-option-vertical', 'NunkiCore' ),
		'glyphicon-paperclip' => __( 'glyphicon glyphicon-paperclip', 'NunkiCore' ),
		'glyphicon-paste' => __( 'glyphicon glyphicon-paste', 'NunkiCore' ),
		'glyphicon-pause' => __( 'glyphicon glyphicon-pause', 'NunkiCore' ),
		'glyphicon-pawn' => __( 'glyphicon glyphicon-pawn', 'NunkiCore' ),
		'glyphicon-pencil' => __( 'glyphicon glyphicon-pencil', 'NunkiCore' ),
		'glyphicon-phone' => __( 'glyphicon glyphicon-phone', 'NunkiCore' ),
		'glyphicon-phone-alt' => __( 'glyphicon glyphicon-phone-alt', 'NunkiCore' ),
		'glyphicon-picture' => __( 'glyphicon glyphicon-picture', 'NunkiCore' ),
		'glyphicon-piggy-bank' => __( 'glyphicon glyphicon-piggy-bank', 'NunkiCore' ),
		'glyphicon-plane' => __( 'glyphicon glyphicon-plane', 'NunkiCore' ),
		'glyphicon-play' => __( 'glyphicon glyphicon-play', 'NunkiCore' ),
		'glyphicon-play-circle' => __( 'glyphicon glyphicon-play-circle', 'NunkiCore' ),
		'glyphicon-plus' => __( 'glyphicon glyphicon-plus', 'NunkiCore' ),
		'glyphicon-plus-sign' => __( 'glyphicon glyphicon-plus-sign', 'NunkiCore' ),
		'glyphicon-print' => __( 'glyphicon glyphicon-print', 'NunkiCore' ),
		'glyphicon-pushpin' => __( 'glyphicon glyphicon-pushpin', 'NunkiCore' ),
		'glyphicon-qrcode' => __( 'glyphicon glyphicon-qrcode', 'NunkiCore' ),
		'glyphicon-queen' => __( 'glyphicon glyphicon-queen', 'NunkiCore' ),
		'glyphicon-question-sign' => __( 'glyphicon glyphicon-question-sign', 'NunkiCore' ),
		'glyphicon-random' => __( 'glyphicon glyphicon-random', 'NunkiCore' ),
		'glyphicon-record' => __( 'glyphicon glyphicon-record', 'NunkiCore' ),
		'glyphicon-refresh' => __( 'glyphicon glyphicon-refresh', 'NunkiCore' ),
		'glyphicon-registration-mark' => __( 'glyphicon glyphicon-registration-mark', 'NunkiCore' ),
		'glyphicon-remove' => __( 'glyphicon glyphicon-remove', 'NunkiCore' ),
		'glyphicon-remove-circle' => __( 'glyphicon glyphicon-remove-circle', 'NunkiCore' ),
		'glyphicon-remove-sign' => __( 'glyphicon glyphicon-remove-sign', 'NunkiCore' ),
		'glyphicon-repeat' => __( 'glyphicon glyphicon-repeat', 'NunkiCore' ),
		'glyphicon-resize-full' => __( 'glyphicon glyphicon-resize-full', 'NunkiCore' ),
		'glyphicon-resize-horizontal' => __( 'glyphicon glyphicon-resize-horizontal', 'NunkiCore' ),
		'glyphicon-resize-small' => __( 'glyphicon glyphicon-resize-small', 'NunkiCore' ),
		'glyphicon-resize-vertical' => __( 'glyphicon glyphicon-resize-vertical', 'NunkiCore' ),
		'glyphicon-retweet' => __( 'glyphicon glyphicon-retweet', 'NunkiCore' ),
		'glyphicon-road' => __( 'glyphicon glyphicon-road', 'NunkiCore' ),
		'glyphicon-rub' => __( 'glyphicon glyphicon-rub', 'NunkiCore' ),
		'glyphicon-ruble' => __( 'glyphicon glyphicon-ruble', 'NunkiCore' ),
		'glyphicon-save' => __( 'glyphicon glyphicon-save', 'NunkiCore' ),
		'glyphicon-saved' => __( 'glyphicon glyphicon-saved', 'NunkiCore' ),
		'glyphicon-save-file' => __( 'glyphicon glyphicon-save-file', 'NunkiCore' ),
		'glyphicon-scale' => __( 'glyphicon glyphicon-scale', 'NunkiCore' ),
		'glyphicon-scissors' => __( 'glyphicon glyphicon-scissors', 'NunkiCore' ),
		'glyphicon-screenshot' => __( 'glyphicon glyphicon-screenshot', 'NunkiCore' ),
		'glyphicon-sd-video' => __( 'glyphicon glyphicon-sd-video', 'NunkiCore' ),
		'glyphicon-search' => __( 'glyphicon glyphicon-search', 'NunkiCore' ),
		'glyphicon-send' => __( 'glyphicon glyphicon-send', 'NunkiCore' ),
		'glyphicon-share' => __( 'glyphicon glyphicon-share', 'NunkiCore' ),
		'glyphicon-share-alt' => __( 'glyphicon glyphicon-share-alt', 'NunkiCore' ),
		'glyphicon-shopping-cart' => __( 'glyphicon glyphicon-shopping-cart', 'NunkiCore' ),
		'glyphicon-signal' => __( 'glyphicon glyphicon-signal', 'NunkiCore' ),
		'glyphicon-sort' => __( 'glyphicon glyphicon-sort', 'NunkiCore' ),
		'glyphicon-sort-by-alphabet' => __( 'glyphicon glyphicon-sort-by-alphabet', 'NunkiCore' ),
		'glyphicon-sort-by-alphabet-alt' => __( 'glyphicon glyphicon-sort-by-alphabet-alt', 'NunkiCore' ),
		'glyphicon-sort-by-attributes' => __( 'glyphicon glyphicon-sort-by-attributes', 'NunkiCore' ),
		'glyphicon-sort-by-attributes-alt' => __( 'glyphicon glyphicon-sort-by-attributes-alt', 'NunkiCore' ),
		'glyphicon-sort-by-order' => __( 'glyphicon glyphicon-sort-by-order', 'NunkiCore' ),
		'glyphicon-sort-by-order-alt' => __( 'glyphicon glyphicon-sort-by-order-alt', 'NunkiCore' ),
		'glyphicon-sound-5-1' => __( 'glyphicon glyphicon-sound-5-1', 'NunkiCore' ),
		'glyphicon-sound-6-1' => __( 'glyphicon glyphicon-sound-6-1', 'NunkiCore' ),
		'glyphicon-sound-7-1' => __( 'glyphicon glyphicon-sound-7-1', 'NunkiCore' ),
		'glyphicon-sound-dolby' => __( 'glyphicon glyphicon-sound-dolby', 'NunkiCore' ),
		'glyphicon-sound-stereo' => __( 'glyphicon glyphicon-sound-stereo', 'NunkiCore' ),
		'glyphicon-star' => __( 'glyphicon glyphicon-star', 'NunkiCore' ),
		'glyphicon-star-empty' => __( 'glyphicon glyphicon-star-empty', 'NunkiCore' ),
		'glyphicon-stats' => __( 'glyphicon glyphicon-stats', 'NunkiCore' ),
		'glyphicon-step-backward' => __( 'glyphicon glyphicon-step-backward', 'NunkiCore' ),
		'glyphicon-step-forward' => __( 'glyphicon glyphicon-step-forward', 'NunkiCore' ),
		'glyphicon-stop' => __( 'glyphicon glyphicon-stop', 'NunkiCore' ),
		'glyphicon-subscript' => __( 'glyphicon glyphicon-subscript', 'NunkiCore' ),
		'glyphicon-subtitles' => __( 'glyphicon glyphicon-subtitles', 'NunkiCore' ),
		'glyphicon-sunglasses' => __( 'glyphicon glyphicon-sunglasses', 'NunkiCore' ),
		'glyphicon-superscript' => __( 'glyphicon glyphicon-superscript', 'NunkiCore' ),
		'glyphicon-tag' => __( 'glyphicon glyphicon-tag', 'NunkiCore' ),
		'glyphicon-tags' => __( 'glyphicon glyphicon-tags', 'NunkiCore' ),
		'glyphicon-tasks' => __( 'glyphicon glyphicon-tasks', 'NunkiCore' ),
		'glyphicon-tent' => __( 'glyphicon glyphicon-tent', 'NunkiCore' ),
		'glyphicon-text-background' => __( 'glyphicon glyphicon-text-background', 'NunkiCore' ),
		'glyphicon-text-color' => __( 'glyphicon glyphicon-text-color', 'NunkiCore' ),
		'glyphicon-text-height' => __( 'glyphicon glyphicon-text-height', 'NunkiCore' ),
		'glyphicon-text-size' => __( 'glyphicon glyphicon-text-size', 'NunkiCore' ),
		'glyphicon-text-width' => __( 'glyphicon glyphicon-text-width', 'NunkiCore' ),
		'glyphicon-th' => __( 'glyphicon glyphicon-th', 'NunkiCore' ),
		'glyphicon-th-large' => __( 'glyphicon glyphicon-th-large', 'NunkiCore' ),
		'glyphicon-th-list' => __( 'glyphicon glyphicon-th-list', 'NunkiCore' ),
		'glyphicon-thumbs-down' => __( 'glyphicon glyphicon-thumbs-down', 'NunkiCore' ),
		'glyphicon-thumbs-up' => __( 'glyphicon glyphicon-thumbs-up', 'NunkiCore' ),
		'glyphicon-time' => __( 'glyphicon glyphicon-time', 'NunkiCore' ),
		'glyphicon-tint' => __( 'glyphicon glyphicon-tint', 'NunkiCore' ),
		'glyphicon-tower' => __( 'glyphicon glyphicon-tower', 'NunkiCore' ),
		'glyphicon-transfer' => __( 'glyphicon glyphicon-transfer', 'NunkiCore' ),
		'glyphicon-trash' => __( 'glyphicon glyphicon-trash', 'NunkiCore' ),
		'glyphicon-tree-conifer' => __( 'glyphicon glyphicon-tree-conifer', 'NunkiCore' ),
		'glyphicon-tree-deciduous' => __( 'glyphicon glyphicon-tree-deciduous', 'NunkiCore' ),
		'glyphicon-triangle-bottom' => __( 'glyphicon glyphicon-triangle-bottom', 'NunkiCore' ),
		'glyphicon-triangle-left' => __( 'glyphicon glyphicon-triangle-left', 'NunkiCore' ),
		'glyphicon-triangle-right' => __( 'glyphicon glyphicon-triangle-right', 'NunkiCore' ),
		'glyphicon-triangle-top' => __( 'glyphicon glyphicon-triangle-top', 'NunkiCore' ),
		'glyphicon-unchecked' => __( 'glyphicon glyphicon-unchecked', 'NunkiCore' ),
		'glyphicon-upload' => __( 'glyphicon glyphicon-upload', 'NunkiCore' ),
		'glyphicon-usd' => __( 'glyphicon glyphicon-usd', 'NunkiCore' ),
		'glyphicon-user' => __( 'glyphicon glyphicon-user', 'NunkiCore' ),
		'glyphicon-volume-down' => __( 'glyphicon glyphicon-volume-down', 'NunkiCore' ),
		'glyphicon-volume-off' => __( 'glyphicon glyphicon-volume-off', 'NunkiCore' ),
		'glyphicon-volume-up' => __( 'glyphicon glyphicon-volume-up', 'NunkiCore' ),
		'glyphicon-warning-sign' => __( 'glyphicon glyphicon-warning-sign', 'NunkiCore' ),
		'glyphicon-wrench' => __( 'glyphicon glyphicon-wrench', 'NunkiCore' ),
		'glyphicon-xbt' => __( 'glyphicon glyphicon-xbt', 'NunkiCore' ),
		'glyphicon-yen' => __( 'glyphicon glyphicon-yen', 'NunkiCore' ),
		'glyphicon-zoom-in' => __( 'glyphicon glyphicon-zoom-in', 'NunkiCore' ),
		'glyphicon-zoom-out' => __( 'glyphicon glyphicon-zoom-out', 'NunkiCore' )
	);

	// Typography Defaults
	$typodefault = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => false
	);

	$onlyfont = array(
		'sizes' => false,
		'face' => 'georgia',
		'style' => 'bold',
		'color' => false
	);

	$sizecolorh1 = array(
		'sizes' => '36px',
		'face' => false,
		'style' => false,
		'color' => false
	);

	// License type
	$license_options = array(
		'cc-sa' => __( 'cc-sa', 'NunkiCore' ),
		'cc-nc-eu' => __( 'cc-nc-eu', 'NunkiCore' ),
		'cc-zero' => __( 'cc-zero', 'NunkiCore' ),
		'cc-share' => __( 'cc-share', 'NunkiCore' ),
		'cc-by' => __( 'cc-by', 'NunkiCore' ),
		'cc-nc-jp' => __( 'cc-nc-jp', 'NunkiCore' ),
		'cc-pd' => __( 'cc-pd', 'NunkiCore' ),
		'cc-cc' => __( 'cc-cc', 'NunkiCore' ),
		'cc-sampling' => __( 'cc-sampling', 'NunkiCore' ),
		'cc-pd-alt' => __( 'cc-pd-alt', 'NunkiCore' ),
		'cc-nd' => __( 'cc-nd', 'NunkiCore' ),
		'cc-nc' => __( 'cc-nc', 'NunkiCore' ),
		'cc-remix' => __( 'cc-remix', 'NunkiCore' )
	);

	// Mostrar listado de categorias
	$categories = array();
	$categories_obj = get_categories();
	foreach ($categories_obj as $category) {
		$categories[$category->cat_ID] = $category->cat_name;
	}

	// Numero de entradas en slider
	$numpostslide = array(
		'1' => __( 'Uno', 'NunkiCore' ),
		'2' => __( 'Dos', 'NunkiCore' ),
		'3' => __( 'Tres', 'NunkiCore' ),
		'4' => __( 'Cuatro', 'NunkiCore' ),
		'5' => __( 'Cinco', 'NunkiCore' )
	);

	// Numero de entradas en secciones
	$numpostsection = array(
		'1' => __( 'Un', 'NunkiCore' ),
		'2' => __( 'Dos', 'NunkiCore' ),
		'3' => __( 'Tres', 'NunkiCore' ),
		'4' => __( 'Quatre', 'NunkiCore' ),
		'5' => __( 'Cinc', 'NunkiCore' ),
		'6' => __( 'Sis', 'NunkiCore' )
	);

	$branchgitlab = array(
		'master' => __( 'Master', 'NunkiCore' ),
		'dev' => __( 'Dev', 'NunkiCore' )
	);

	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages( 'sort_column=post_parent,menu_order' );
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/inc/NunkiCore/images/';

	$options = array();

	/* NOTA -> Traducción con varible $nameTheme?
	/* ######################################################################## */
	/* 							THEME OPTIONS									*/
	/* ######################################################################## */
	require plugin_dir_path( __FILE__ ) . 'inc/NunkiCore/nunki-settings.php';

	/* #####################
	/* 		GENERAL SETTINGS
	/* ####################################################### */
	$options[] = array(
		'name' => __( 'General Settings', 'NunkiCore' ),
		'type' => 'heading'
	);

	if ($ThemeStylesheet == 1) {
		$options[] = array(
			'name' => __( 'Theme Stylesheet', 'NunkiCore' ),
			'desc' => __( 'Select your themes alternative color scheme.', 'NunkiCore' ),
			'id' => 'style',
			'std' => 'style.min.css',
			'type' => 'select',
			'options' => $style
		);
	}

	/* ################################ HEADER ###*/
	$options[] = array(
		'name' => __( 'Header', 'NunkiCore' ),
		'class' => 'header-section'
	);

	/*$options[] = array(
		'name' =>  __( 'Custom background header', 'NunkiCore' ),
		'desc' => __( 'Change background header. Default color: #F1F1F1.', 'NunkiCore' ),
		'id' => 'backgroundcolorheader',
		'std' => $background_default,
		'type' => 'background'
	);*/

	/* Nota -> no tiene que ser type background, sino solo color */
	/*$options[] = array(
		'name' =>  __( 'Custom color header', 'NunkiCore' ),
		'desc' => __( 'Change text color header. Default color: #151515.', 'NunkiCore' ),
		'id' => 'textcolorheader',
		'std' => $background_defaul,
		'type' => 'background'
	);*/


	/*$options[] = array(
		'name' =>  __( 'Background page', 'NunkiCore' ),
		'desc' => __( 'Change background page (image or color). Default color: #F1F1F1.', 'NunkiCore' ),
		'id' => 'backgroundpage',
		'std' => $background_defaults,
		'type' => 'background'
	);*/

	if ($Favicon == 1) {
		$options[] = array(
			'name' => __( 'Custom Favicon', 'NunkiCore' ),
			'desc' => __( 'Upload a 16px x 16px Png/Gif image that will represent your website\'s favicon.', 'NunkiCore' ),
			'id' => 'favicon',
			'type' => 'upload'
		);
	}

	if ($TrackingCode == 1) {
		$options[] = array(
			'name' => __( 'Tracking code', 'NunkiCore' ),
			'desc' => __( 'Add tracking code (Google Analytics or Piwik)', 'NunkiCore' ),
			'id' => 'trackingcode',
			'std' => '',
			'type' => 'textarea'
		);
	}

	if ($LayoutStyle) {
		$options[] = array(
			'name' => "Layout Style",
			'desc' => "Here you can define layout style",
			'id' => "sidebar_position",
			'std' => "sidebar_left",
			'type' => "images",
			'options' => array(
				'sidebar_left' => $imagepath . '2cl.png',
				'sidebar_right' => $imagepath . '2cr.png'
			)
		);
	}

	/* ################################ GOOGLE ADS ###*/
	$options[] = array(
		'name' => __( 'Google Ads', 'NunkiCore' ),
		'class' => 'header-section'
	);

	if ($Ads) {
		$options[] = array(
			'name' => __( 'Ads', 'NunkiCore' ),
			'desc' => __( 'Add code from Google Adsense.', 'NunkiCore' ),
			'id' => 'ads',
			'std' => '',
			'type' => 'textarea'
		);
	}

	/* ################################ ADVANCED SETTINGS ###*/
	$options[] = array(
		'name' => __( 'Advanced Settings', 'NunkiCore' ),
		'class' => 'header-section'
	);

	if ($CookiesNotice) {
		$options[] = array(
		    'name' => __( 'Cookie Notice', 'NunkiCore' ),
		    'desc' => __( 'Activate / Deactivate cookies notice', 'NunkiCore' ),
		    'id' => 'cookies',
		    'std' => '0',
		    'type' => 'checkbox'
		);

		$options[] = array(
		    'name' => __( 'Message from Home Page', 'NunkiCore' ),
		    'desc' => __( 'Add message from only home page', 'NunkiCore' ),
		    'id' => 'cookieshome',
		    'std' => 'Utilizamos cookies propias y de terceros para mejorar nuestros servicios. Si continúa navegando, consideramos que acepta su uso. Doble clic sobre el triángulo azul para cerrar.',
		    'type' => 'textarea',
		    'class' => 'hidden'
		);

		$options[] = array(
		    'name' => __( 'Message from all pages', 'NunkiCore' ),
		    'desc' => __( 'Add message from all pages (except home page)', 'NunkiCore' ),
		    'id' => 'cookiesall',
		    'std' => 'Utilizamos cookies propias y de terceros para mejorar nuestros servicios. Si continúa navegando, consideramos que acepta su uso. Doble clic sobre aquí para cerrar.',
		    'type' => 'textarea',
		    'class' => 'hidden'
		);
	}

	if ($BreadCrumb) {
		$options[] = array(
			'name' => __( 'Breadcrumbs', 'NunkiCore' ),
			'desc' => __( 'Activate / Deactivate breadcrumbs in post page.', 'NunkiCore' ),
			'id' => 'breadcrumb',
			'std' => '0',
			'type' => 'checkbox'
		);
	}

	if ($ToTopEffect) {
		$options[] = array(
			'name' => __( 'To top effect', 'NunkiCore' ),
			'desc' => __( 'Activate / Deactivate "Back to Top" link in footer', 'NunkiCore' ),
			'id' => 'totop',
			'std' => '0',
			'type' => 'checkbox'
		);
	}

	if ($RandomPosts) {
		$options[] = array(
			'name' => __( 'Random post section', 'NunkiCore' ),
			'desc' => __( 'Display 3 random titles post', 'NunkiCore' ),
			'id' => 'rdmpost',
			'std' => '0',
			'type' => 'checkbox'
		);
	}

	if ($CollaborateMessage) {
		$options[] = array(
			'name' => __( 'Announcement to collaborate', 'NunkiCore' ),
			'desc' => __( 'Activate / Deactivate announcement to collaborate', 'NunkiCore' ),
			'id' => 'collaborate',
			'std' => '0',
			'type' => 'checkbox'
		);

		$options[] = array(
			'name' => __( 'URL page collaborate', 'NunkiCore' ),
			'desc' => __( 'Add the URL of your page collaboration.', 'NunkiCore' ),
			'id' => 'collaborateurl',
			'std' => '#',
			'class' => 'hidden',
			'type' => 'text'
		);
	}

	if ($RelatedPosts) {
		$options[] = array(
			'name' => __( 'Related Post', 'NunkiCore' ),
			'desc' => __( 'Activate / Deactivate display related posts before comments', 'NunkiCore' ),
			'id' => 'relatedposts',
			'std' => '0',
			'type' => 'checkbox'
		);
	}

	if ($ShareContent) {
		$options[] = array(
			'name' => __( 'Share content', 'NunkiCore' ),
			'desc' => __( 'Activate / Deactivate share content in single pages (Facebook, Twitter, Google+ and E-mail).', 'NunkiCore' ),
			'id' => 'sharecontent',
			'std' => '0',
			'type' => 'checkbox'
		);
	}

	/* ###########
	/* 		SECTIONS
	/* ####################################################### */

	if ($SectionsHome == 1) {
		$options[] = array(
			'name' => __( 'Sections', 'NunkiCore' ),
			'type' => 'heading'
		);

		$options[] = array(
			'name' => __( 'Section 1', 'NunkiCore' ),
			'class' => 'header-section'
		);

		if ( $categories ) {
			$options[] = array(
				'name' => __( 'Select category for this section', 'NunkiCore' ),
				'desc' => __( 'Indicates the category.', 'NunkiCore' ),
				'id' => 'catsecone',
				'type' => 'select',
				'options' => $categories
			);
		}

		$options[] = array(
			'name' => __( 'Number of entries in this section', 'NunkiCore' ),
			'desc' => __( 'Indicates the number.', 'NunkiCore' ),
			'id' => 'numpostsecone',
			'std' => '4',
			'type' => 'select',
			'options' => $numpostsection
		);

		$options[] = array(
			'name' => __( 'Section 2', 'NunkiCore' ),
			'class' => 'header-section'
		);

		if ( $categories ) {
			$options[] = array(
				'name' => __( 'Select category for this section', 'NunkiCore' ),
				'desc' => __( 'Indicates the category.', 'NunkiCore' ),
				'id' => 'catsectwo',
				'type' => 'select',
				'options' => $categories
			);
		}

		$options[] = array(
			'name' => __( 'Number of entries in this section', 'NunkiCore' ),
			'desc' => __( 'Indicates the number.', 'NunkiCore' ),
			'id' => 'numpostsectwo',
			'std' => '4',
			'type' => 'select',
			'options' => $numpostsection
		);

		$options[] = array(
			'name' => __( 'Section 3', 'NunkiCore' ),
			'class' => 'header-section'
		);

		if ( $categories ) {
			$options[] = array(
				'name' => __( 'Select category for this section', 'NunkiCore' ),
				'desc' => __( 'Indicates the category.', 'NunkiCore' ),
				'id' => 'catsecthree',
				'type' => 'select',
				'options' => $categories
			);
		}

		$options[] = array(
			'name' => __( 'Number of entries in this section', 'NunkiCore' ),
			'desc' => __( 'Indicates the number.', 'NunkiCore' ),
			'id' => 'numpostsecthree',
			'std' => '4',
			'type' => 'select',
			'options' => $numpostsection
		);

		$options[] = array(
			'name' => __( 'Section 4', 'NunkiCore' ),
			'class' => 'header-section'
		);

		if ( $categories ) {
			$options[] = array(
				'name' => __( 'Select category for this section', 'NunkiCore' ),
				'desc' => __( 'Indicates the category.', 'NunkiCore' ),
				'id' => 'catsecfour',
				'type' => 'select',
				'options' => $categories
			);
		}

		$options[] = array(
			'name' => __( 'Number of entries in this section', 'NunkiCore' ),
			'desc' => __( 'Indicates the number.', 'NunkiCore' ),
			'id' => 'numpostsecfour',
			'std' => '4',
			'type' => 'select',
			'options' => $numpostsection
		);

		$options[] = array(
			'name' => __( 'Section 5', 'NunkiCore' ),
			'class' => 'header-section'
		);

		if ( $categories ) {
			$options[] = array(
				'name' => __( 'Select category for this section', 'NunkiCore' ),
				'desc' => __( 'Indicates the category.', 'NunkiCore' ),
				'id' => 'catsecfive',
				'type' => 'select',
				'options' => $categories
			);
		}

		$options[] = array(
			'name' => __( 'Number of entries in this section', 'NunkiCore' ),
			'desc' => __( 'Indicates the number.', 'NunkiCore' ),
			'id' => 'numpostsecfive',
			'std' => '4',
			'type' => 'select',
			'options' => $numpostsection
		);

		$options[] = array(
			'name' => __( 'Section 6', 'NunkiCore' ),
			'class' => 'header-section'
		);

		if ( $categories ) {
			$options[] = array(
				'name' => __( 'Select category for this section', 'NunkiCore' ),
				'desc' => __( 'Indicates the category.', 'NunkiCore' ),
				'id' => 'catsecsix',
				'type' => 'select',
				'options' => $categories
			);
		}

		$options[] = array(
			'name' => __( 'Number of entries in this section', 'NunkiCore' ),
			'desc' => __( 'Indicates the number.', 'NunkiCore' ),
			'id' => 'numpostsecsix',
			'std' => '4',
			'type' => 'select',
			'options' => $numpostsection
		);
	}

	/* ###########
	/* 		HEADER (necesario?)
	/* ####################################################### */
	$options[] = array(
		'name' => __( 'Header', 'NunkiCore' ),
		'type' => 'heading'
	);

	if ($HeaderBackground) {
		$options[] = array(
			'name' => __( 'Cambiar imagen de la cabecera', 'NunkiCore' ),
			'desc' => __( 'This creates a full size uploader that previews the image.', 'NunkiCore' ),
			'id' => 'imgheader',
			'type' => 'upload'
		);
	}

	if ($HeaderGlyphicons) {
		$options[] = array(
			'name' => __( 'Glyphicons', 'NunkiCore' ),
			'desc' => __( 'Cambia el Glyphicon del titulo principal.', 'NunkiCore' ),
			'id' => 'glyphicon',
			'std' => 'glyphicon glyphicon-flash',
			'type' => 'select',
			'options' => $glyphicons
		);
	}

	if ($HeaderBrand) {
		$options[] = array(
			'name' => __( 'Título principal de la web: Primera palabra', 'NunkiCore' ),
			'desc' => __( 'En el menu principal nuestra el titulo principal del blog en dos colores.', 'NunkiCore' ),
			'id' => 'firstword',
			'std' => 'Alpheratz',
			'class' => 'mini',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Título principal de la web: Segunda palabra', 'NunkiCore' ),
			'desc' => __( 'En el menu principal nuestra el titulo principal del blog en dos colores.', 'NunkiCore' ),
			'id' => 'secondword',
			'std' => 'Theme',
			'class' => 'mini',
			'type' => 'text'
		);
	}

	/* ###########
	/* 		SLIDER
	/* ####################################################### */

	if ($Slider) {
		$options[] = array(
			'name' => __( 'Slider', 'NunkiCore' ),
			'type' => 'heading'
		);

		$options[] = array(
			'name' => __( 'Marca esta opción para activar el Slider', 'NunkiCore' ),
			'desc' => __( 'Haz clic para ver más opciones.', 'NunkiCore' ),
			'id' => 'slideon',
			'type' => 'checkbox'
		);

		if ( $categories ) {
			$options[] = array(
				'name' => __( 'Selecciona la categoría a mostrar en el slider', 'NunkiCore' ),
				'desc' => __( 'Passed an array of categories with cat_ID and cat_name', 'NunkiCore' ),
				'id' => 'categoriaslider',
				'type' => 'select',
				'class' => 'hidden',
				'options' => $categories
			);
		}

		$options[] = array(
			'name' => __( 'Número de entradas a mostrar', 'NunkiCore' ),
			'desc' => __( 'Indica el número.', 'NunkiCore' ),
			'id' => 'numpost',
			'std' => 'tres',
			'type' => 'select',
			'class' => 'hidden',
			'options' => $numpostslide
		);

		$options[] = array(
			'name' => __( 'Intervalo entre diapositivas', 'NunkiCore' ),
			'desc' => __( 'Número en segundos del tiempo de durada entre diapositivas.', 'NunkiCore' ),
			'id' => 'interval',
			'std' => '2000',
			'class' => 'mini',
			'class' => 'hidden',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Pausar slider', 'NunkiCore' ),
			'desc' => __( 'Marca para pausar slider cuando el cursor está encima del slider.', 'NunkiCore' ),
			'id' => 'pauseonhover',
			'class' => 'hidden',
			'type' => 'checkbox'
		);
	}

	/* ###############
	/* 		TYPOGRAPHY
	/* ####################################################### */
	$options[] = array(
		'name' => __( 'Typography', 'NunkiCore' ),
		'type' => 'heading'
	);

	$options[] = array( 'name' => __( 'Body Typography Settings', 'NunkiCore' ),
		'desc' => __( ' ', 'NunkiCore' ),
		'id' => 'typobody',
		'std' => $typodefault,
		'type' => 'typography'
	);
/*
	$options[] = array( 'name' => __( 'Heading Typography Settings', 'NunkiCore' ),
		'desc' => __( ' ', 'NunkiCore' ),
		'id' => 'typobody',
		'std' => $typodefault,
		'type' => 'typography'
	);

	$options[] = array(
		'name' => __( 'Typography for headers', 'NunkiCore' ),
		'desc' => __( ' ', 'NunkiCore' ),
		'id' => "custom_typography",
		'std' => $typodefault,
		'type' => 'typography',
		'options' => $onlyfont
	);



	$options[] = array(
		'name' => __( 'H1 Typography', 'NunkiCore' ),
		'desc' => __( ' ', 'NunkiCore' ),
		'id' => "typoh1",
		'std' => $typodefault,
		'type' => 'typography',
		'options' => $sizecolorh1
	);
*/


	/* ####################
	/* 		INFINITE SCROLL
	/* ####################################################### */

	if ($InfiniteScroll) {
		$options[] = array(
			'name' => __( 'Infinite Scroll', 'NunkiCore' ),
			'type' => 'heading'
		);

		$options[] = array(
			'name' => __( 'Activar "Infinite Scroll"', 'NunkiCore' ),
			'desc' => __( 'Activa este efecto para que carguen las entradas automáticamente.', 'NunkiCore' ),
			'id' => 'infinitescroll',
			'std' => '1',
			'type' => 'checkbox'
		);

		$options[] = array(
			'name' => __( 'Selector del contenedor', 'NunkiCore' ),
			'desc' => __( 'DIV que contiene el contenido de las entradas en tu tema.', 'NunkiCore' ),
			'id' => 'contentselector',
			'std' => '#main',
			'class' => 'mini',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Selector del navegador', 'NunkiCore' ),
			'desc' => __( 'DIV que contiene el paginador de tu tema', 'NunkiCore' ),
			'id' => 'navselector',
			'std' => '.navigation',
			'class' => 'mini',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Selector de siguiente página', 'NunkiCore' ),
			'desc' => __( 'Enlace del paginador para pasar página.', 'NunkiCore' ),
			'id' => 'nextselector',
			'std' => 'li a.next',
			'class' => 'mini',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Selector del post', 'NunkiCore' ),
			'desc' => __( 'DIV que contiene el post individual.', 'NunkiCore' ),
			'id' => 'itemselector',
			'std' => 'article',
			'class' => 'mini',
			'type' => 'text'
		);
	}

	/* ###################
	/* 		SOCIAL OPTIONS
	/* ####################################################### */

	if ($SocialOptions) {
		$options[] = array(
			'name' => __( 'Social Options', 'NunkiCore' ),
			'type' => 'heading'
		);

		$options[] = array(
			'name' => __( 'Twitter', 'NunkiCore' ),
			'desc' => __( 'Add social icon.', 'NunkiCore' ),
			'id' => 'social_tw',
			'std' => '#',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Facebook', 'NunkiCore' ),
			'desc' => __( 'Add social icon.', 'NunkiCore' ),
			'id' => 'social_fb',
			'std' => '#',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Google+', 'NunkiCore' ),
			'desc' => __( 'Add social icon.', 'NunkiCore' ),
			'id' => 'social_gp',
			'std' => '#',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'GitHub', 'NunkiCore' ),
			'desc' => __( 'Add social icon.', 'NunkiCore' ),
			'id' => 'social_gh',
			'std' => '#',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Telegram Channel', 'NunkiCore' ),
			'desc' => __( 'Add social icon.', 'NunkiCore' ),
			'id' => 'social_tg',
			'std' => '#',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'Feed / RSS', 'NunkiCore' ),
			'desc' => __( 'Add social icon.', 'NunkiCore' ),
			'id' => 'social_rs',
			'std' => '#',
			'type' => 'text'
		);

		$options[] = array(
			'name' => __( 'License Content Link', 'NunkiCore' ),
			'desc' => __( 'Add license icon.', 'NunkiCore' ),
			'id' => 'social_cc',
			'std' => '#',
			'type' => 'text'
		);
	}

	/* ############
	/* 		LICENSE
	/* ####################################################### */

	if ($SelectLicense) {
		$options[] = array(
			'name' => __( 'License', 'NunkiCore' ),
			'type' => 'heading'
		);

		$options[] = array(
			'name' => __( 'Select license ', 'NunkiCore' ),
			'desc' => __( 'Choose your content license. More info: http://cc-icons.github.io/icons/', 'NunkiCore' ),
			'id' => 'license',
			'std' => $license_options, // These items get checked by default
			'type' => 'multicheck',
			'options' => $license_options
		);
	}

	/* ###################
	/* 		FOOTER OPTIONS
	/* ####################################################### */

	if ($FooterOptions) {
		$options[] = array(
			'name' => __( 'Footer Options', 'NunkiCore' ),
			'type' => 'heading'
		);

		$options[] = array(
			'name' => __( 'Copyright', 'NunkiCore' ),
			'desc' => __( 'Para añadir el Copyright, aparece en la parte inferior derecha junto al nombre de la plantilla. Puedes usar etiquetas HTML como <strong> o <font>.', 'NunkiCore' ),
			'id' => 'copyright',
			'std' => 'Nombre Autor',
			'type' => 'textarea'
		);
	}

	/* ##################
	/* 		THEME UPDATES
	/* ####################################################### */

	if ($ThemeUpdate) {
		$options[] = array(
			'name' => __( 'Theme Updates', 'NunkiCore' ),
			'type' => 'heading'
		);

		$options[] = array(
			'name' => __( 'Enable Themes Updates', 'NunkiCore' ),
			'desc' => __( 'Check to enable updates', 'NunkiCore' ),
			'id' => 'enableupdates',
			'std' => '1',
			'type' => 'checkbox'
		);

		$options[] = array(
			'name' => __( 'Branch name', 'NunkiCore' ),
			'desc' => __( 'Indicates the branch from which you want to update. <strong><u>WARNING</u></strong>: Always use the master branch to avoid problems. If you decide to use another branch is <strong> under your responsibility </strong>.', 'NunkiCore' ),
			'id' => 'branch',
			'std' => 'master',
			'type' => 'select',
			'options' => $branchgitlab
		);
	}

	/* ###########
	/* 		OTHERS
	/* ####################################################### */
	$options[] = array(
		'name' => __( 'Others', 'NunkiCore' ),
		'type' => 'heading'
	);

	if ($ChristmassTree) {
		$options[] = array(
			'name' => __( 'Activar Navidad :D', 'NunkiCore' ),
			'desc' => __( 'Añade un árbol de navidad en el footer', 'NunkiCore' ),
			'id' => 'christmas',
			'std' => '0',
			'type' => 'checkbox'
		);
	}

	if ($CSSExtra) {
		$options[] = array(
			'name' => __( 'CSS extra', 'NunkiCore' ),
			'desc' => __( 'Añade CSS extra aquí.', 'NunkiCore' ),
			'id' => 'cssextra',
			'std' => '/* CSS EXTRA */',
			'type' => 'textarea'
		);

	}

	if ($JSExtra) {
		$options[] = array(
			'name' => __( 'JS extra', 'NunkiCore' ),
			'desc' => __( 'Añade JS extra aquí. Perfecto para campañas externas donde añadir un simple código JS.', 'NunkiCore' ),
			'id' => 'jsextra',
			'std' => '/* JS EXTRA */',
			'type' => 'textarea'
		);
	}


	/* ################################################################################# */

	/*
	$options[] = array(
		'name' => __( 'Categorías excluídas', 'NunkiCore' ),
		'desc' => __( 'Escribe "-ID" de las categorias que quieres excluir de la pagina principal', 'NunkiCore' ),
		'id' => 'excludecategory',
		'std' => '',
		'type' => 'text'
	);
	*/

	return $options;
}
