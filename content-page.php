<article <?php post_class();?>>
    <div class="post-row row">
        <div class="col-md-12 p-cont">
			<h2><?php the_title(); ?></h2>
        </div>
		<div class="col-md-12">
            <div class="entry">
                <?php the_content(); ?>
            </div><!-- .entry -->
		</div><!-- .col-md-12 -->
    </div><!-- .post-row -->
</article>
