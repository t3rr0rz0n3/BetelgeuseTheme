<article <?php post_class();?>>
    <div class="date"><?php the_date('l, d \d\e F Y'); ?></div>
    <div class="post-row row">
        <div class="col-md-3">
            <h2 class="blogname">
                <?php
                    $category = get_the_category();
                    if ($category) {
                        echo '<a href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "Ver todas las entradas de %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
                    }
                ?>
            </h2>
        </div><!-- .col-md-3 -->
        <div class="col-md-9">
            <h2 class="blogtitle col-md-9">
                <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><?php the_title(); ?></a>
            </h2>
        </div><!-- .col-md-9 -->
    </div><!-- .post-row -->
</article>
