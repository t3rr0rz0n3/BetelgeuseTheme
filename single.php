<?php get_header(); ?>
                            <!-- #PRIMARY, las entradas, la entrada, la pagina, etc.. -->
                            <div id="primary" class="col-md-12 single">
                                <main id="main">
                                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                                        <?php get_template_part( 'content-single', get_post_format() ); ?>

                                    <?php endwhile; else: ?>

                                        <div class="">
                                            <?php get_template_part( '404'); ?>
                                        </div>

                                    <?php endif; ?>

                                </main>

                                <div class="sidebar-footer">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-4">
                                                <center><img src="<?php bloginfo('template_url')?>/img/CloseWindowsOpenDoors.png" alt="Close Windows Open Doors" /></center>
                                            </div><!-- .col-md-4 -->
                                            <div class="col-md-4">
                                                <center><img src="<?php bloginfo('template_url')?>/img/DRMToxic.png" alt="DRM Toxic" /></center>
                                            </div><!-- .col-md-4 -->
                                            <div class="col-md-4">
                                                <center><img src="<?php bloginfo('template_url')?>/img/DualBootWin10.png" alt="Dual Boot Win 10" /></center>
                                            </div><!-- .col-md-4 -->
                                        </div><!-- .col-md-12 -->
                                    </div><!-- .row -->
                                </div><!-- #secondary -->
                            </div><!-- #primary -->
<?php get_footer(); ?>
