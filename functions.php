<?php

// Theme Options
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/inc/NunkiCore/' );
require_once dirname( __FILE__ ) . '/inc/NunkiCore/options-framework.php';
require_once dirname( __FILE__ ) . '/inc/NunkiCore/nunki-options.php';

// Loads options.php from child or parent theme
$optionsfile = locate_template( 'options.php' );
load_template( $optionsfile );

// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');

register_nav_menus( array(
    'primary' => __( 'Menu Superior', 'Betelgeuse' ),
) );

register_nav_menus( array(
    'social' => __( 'Menu Social', 'Betelgeuse' ),
) );

// Activamos los thumbnails
add_theme_support('post-thumbnails');
add_image_size('list_articles_thumbs', 350, 270, true );

// Activamos la sidebar lateral
register_sidebar(array(
    'name' => 'SideBar',
    'before_widget' => '<div class="widget">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Activamos la sidebar footer
register_sidebar(array(
    'name' => 'Footer',
    'before_widget' => '<div class="widget col-md-4">',
    'after_widget' => '</div>',
    'before_title' => '<h3>',
    'after_title' => '</h3>'
));

// Active thumbnails
add_theme_support('post-thumbnails');
add_image_size('list_articles_thumbs', 350, 270, true );
the_post_thumbnail();

the_post_thumbnail('thumbnail');       // Thumbnail (default 150px x 150px max)
the_post_thumbnail('medium');          // Medium resolution (default 300px x 300px max)
the_post_thumbnail('large');           // Large resolution (default 640px x 640px max)
the_post_thumbnail('full');            // Original image resolution (unmodified)

// Total de entradas en PL
function totalPost() {
    global $wpdb;
    return (int) $wpdb->get_var('SELECT COUNT(*) FROM ' . $wpdb->posts . ' WHERE post_status = "publish" AND post_type = "post"');
}

function totalCategories() {
    global $wpdb;
    return (int) $wpdb->get_var('SELECT COUNT(*) FROM ' . $wpdb->terms);
}

function topFive() {
    wp_list_categories( array(
        'orderby' => 'count',
        'order' => 'DESC',
        'title_li' => false,
        'show_count' => true,
        'class' => 'test',
        'number' => '5',
        'walker' => new Custom_Walker_Category()
    ) );
}

function ErroRandomFun() {
    $n = rand(0,10);

    switch ($n) {
    case 0:
        return "(·_·)";
        break;
    case 1:
        return "\(o_o)/";
        break;
    case 2:
        return "(·.·)";
        break;
    case 3:
        return "(>_<)";
        break;
    case 4:
        return "(˚Δ˚)b";
        break;
    case 5:
        return "(˚Δ˚)b";
        break;
    case 6:
        return "(;-;)";
        break;
    case 7:
        return "(^_^)b";
        break;
    case 8:
        return "(='X'=)";
        break;
    case 9:
        return "(╯°□°）╯︵ ┻━┻";
        break;
    default:
        return "(>_<)";
        break;
    }
}

// PAGINATION
function custom_pagination() {
    global $wp_query;
    $big = 999999999;
    $pages = paginate_links(array(
        'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format' => '?page=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages,
        'prev_next' => false,
        'type' => 'array',
        'prev_next' => TRUE,
        'prev_text' => '&larr; Previous',
        'next_text' => 'Next &rarr;',
            ));
    if (is_array($pages)) {
        $current_page = ( get_query_var('paged') == 0 ) ? 1 : get_query_var('paged');
        echo '<ul class="pagination">';
        foreach ($pages as $i => $page) {
            if ($current_page == 1 && $i == 0) {
                echo "<li class='active'>$page</li>";
            } else {
                if ($current_page != 1 && $current_page == $i) {
                    echo "<li class='active'>$page</li>";
                } else {
                    echo "<li>$page</li>";
                }
            }
        }
        echo '</ul>';
    }
}

// exclude category "podcast" and "blog"
function BetelgeuseExcludeCatPodcast($query) {
    if ( $query->is_home ) {
        $podcast = of_get_option('excludecategory');
        $query->set( 'cat', $podcast );
    }
    return $query;
}
add_filter( 'pre_get_posts', 'BetelgeuseExcludeCatPodcast' );

// Add Social Network on profile
function BetelgeuseAuthorSocialIcons( $contactmethods ) {

    $contactmethods['twitter'] = 'Twitter URL';
    $contactmethods['facebook'] = 'Facebook URL';
    $contactmethods['googleplus'] = 'Google + URL';
    $contactmethods['correo'] = 'Correo Electrónico';
    $contactmethods['github'] = 'GitHub URL';
    /*$contactmethods['gnusocial'] = 'GNU Social URL';
    $contactmethods['diaspora'] = 'Diaspora* URL';*/

    return $contactmethods;
}
add_filter( 'user_contactmethods', 'BetelgeuseAuthorSocialIcons', 10, 1);

// Social Network in user profile
function BetelgeuseListSocialNetwork() {

    echo '<ul class="icons">';
    $twitter = get_the_author_meta( 'twitter' );
    if ( $twitter && $twitter != '' ) {
        echo '<li><a target="_blank" class="icon-social tw" href="' . esc_url($twitter) . '"><i class="fa fa-twitter"></i></a></li>';
    }

    $facebook = get_the_author_meta( 'facebook' );
    if ( $facebook && $facebook != '' ) {
        echo '<li><a target="_blank" class="icon-social fb" href="' . esc_url($facebook) . '"><i class="fa fa-facebook"></i></a></li>';
    }

    $googleplus = get_the_author_meta( 'googleplus' );
    if ( $googleplus && $googleplus != '' ) {
        echo '<li><a target="_blank" class="icon-social gp" href="' . esc_url($googleplus) . '"><i class="fa fa-google-plus"></i></a></li>';
    }

    $correo = get_the_author_meta( 'correo' );
    if ( $correo && $correo != '' ) {
        echo '<li><a target="_blank" class="icon-social ce" href="mailto:' . esc_url($correo) . '"><i class="fa fa-envelope"></i></a></li>';
    }

    $github = get_the_author_meta( 'github' );
    if ( $github && $github != '' ) {
        echo '<li><a target="_blank" class="icon-social gh" href="' . esc_url($github) . '"><i class="fa fa-github-alt"></i></a></li>';
    }

    /*$gnusocial = get_the_author_meta( 'gnusocial' );
    if ( $gnusocial && $gnusocial != '' ) {
        echo '<li><a target="_blank" class="gs" href="' . esc_url($gnusocial) . '"><span class="sociconoff">GNUSocial</span></a></li>';
    }

    $diaspora = get_the_author_meta( 'diaspora' );
    if ( $diaspora && $diaspora != '' ) {
        echo '<li><a target="_blank" class="dp" href="' . esc_url($diaspora) . '"><span class="sociconoff">Diaspora</span></a></li>';
    }*/
    echo '</ul>';
}

class Custom_Walker_Category extends Walker_Category {

        function start_el( &$output, $category, $depth = 0, $args = array(), $id = 0 ) {
                extract($args);
                $cat_name = esc_attr( $category->name );
                $cat_name = apply_filters( 'list_cats', $cat_name, $category );
                $link = '<a href="' . esc_url( get_term_link($category) ) . '" ';
                if ( $use_desc_for_title == 0 || empty($category->description) )
                        $link .= 'title="' . esc_attr( sprintf(__( 'View all posts filed under %s' ), $cat_name) ) . '"';
                else
                        $link .= 'title="' . esc_attr( strip_tags( apply_filters( 'category_description', $category->description, $category ) ) ) . '"';
                $link .= '>';
                $link .= $cat_name . '</a>';
                if ( !empty($feed_image) || !empty($feed) ) {
                        $link .= ' ';
                        if ( empty($feed_image) )
                                $link .= '(';
                        $link .= '<a href="' . esc_url( get_term_feed_link( $category->term_id, $category->taxonomy, $feed_type ) ) . '"';
                        if ( empty($feed) ) {
                                $alt = ' alt="' . sprintf(__( 'Feed for all posts filed under %s' ), $cat_name ) . '"';
                        } else {
                                $title = ' title="' . $feed . '"';
                                $alt = ' alt="' . $feed . '"';
                                $name = $feed;
                                $link .= $title;
                        }
                        $link .= '>';
                        if ( empty($feed_image) )
                                $link .= $name;
                        else
                                $link .= "<img src='$feed_image'$alt$title" . ' />';
                        $link .= '</a>';
                        if ( empty($feed_image) )
                                $link .= ')';
                }
                if ( !empty($show_count) )
                        $link .= '<span class="badge">'. intval($category->count) .'</span>';
                if ( 'list' == $args['style'] ) {
                        $output .= "\t<li";
                        $class = 'list-group-item';

                        // YOUR CUSTOM CLASS
                        if ($depth)
                            $class .= ' sub-'.sanitize_title_with_dashes($category->name);


                        if ( !empty($current_category) ) {
                                $_current_category = get_term( $current_category, $category->taxonomy );
                                if ( $category->term_id == $current_category )
                                        $class .=  ' current-cat';
                                elseif ( $category->term_id == $_current_category->parent )
                                        $class .=  ' current-cat-parent';
                        }
                        $output .=  ' class="' . $class . '"';
                        $output .= ">$link\n";
                } else {
                        $output .= "\t$link<br />\n";
                }
        } // function start_el

} // class Custom_Walker_Category

// Require plugins notification
function showAdminMessages() {
	$plugin_messages = array();

	include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

    // Download Ninja Forms
    if(!is_plugin_active( 'contact-form-7/wp-contact-form-7.php' )) {
		$plugin_messages[] = 'Betelgeuse Theme requires you to install the Contact Form 7, <a href="https://es.wordpress.org/plugins/ninja-forms/">download it from here</a>.';
	}

	if(count($plugin_messages) > 0)	{
		echo '<div id="message" class="error">';

			foreach($plugin_messages as $message) {
				echo '<p><strong>'.$message.'</strong></p>';
			}

		echo '</div>';
	}
}
add_action('admin_notices', 'showAdminMessages');

/* SCRIPTS NUNKI */
add_action( 'optionsframework_custom_scripts', 'optionsframework_custom_scripts' );
function optionsframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#slideon').click(function() {
  		jQuery('#section-categoriaslider').fadeToggle(400);
	});

	if (jQuery('#slideon:checked').val() !== undefined) {
		jQuery('#section-categoriaslider').show();
	}

});
</script>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#slideon').click(function() {
  		jQuery('#section-interval').fadeToggle(400);
	});

	if (jQuery('#slideon:checked').val() !== undefined) {
		jQuery('#section-interval').show();
	}

});
</script>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#slideon').click(function() {
  		jQuery('#section-pauseonhover').fadeToggle(400);
	});

	if (jQuery('#slideon:checked').val() !== undefined) {
		jQuery('#section-pauseonhover').show();
	}

});
</script>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#slideon').click(function() {
  		jQuery('#section-numpost').fadeToggle(400);
	});

	if (jQuery('#slideon:checked').val() !== undefined) {
		jQuery('#section-numpost').show();
	}

});
</script>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#cookies').click(function() {
  		jQuery('#section-cookieshome').fadeToggle(400);
	});

	if (jQuery('#cookies').val() !== undefined) {
		jQuery('#section-cookieshome').show();
	}

});
</script>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#cookies').click(function() {
  		jQuery('#section-cookiesall').fadeToggle(400);
	});

	if (jQuery('#cookies').val() !== undefined) {
		jQuery('#section-cookiesall').show();
	}

});
</script>


<!-- FADE ERROR NOTICE UPDATE NOTICE -->
<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#setting-error-save_options').hide('2000');

});
</script>

<?php
}

?>
