<?php get_header(); ?>
                            <!-- #PRIMARY, las entradas, la entrada, la pagina, etc.. -->
                            <div id="primary" class="col-md-8 page">
                                <main id="main">
                                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                                        <?php get_template_part( 'content-page', get_post_format() ); ?>

                                    <?php endwhile; else: ?>

                                        <?php get_template_part( '404'); ?>

                                    <?php endif; ?>
                                </main>
                            </div><!-- #primary -->

                            <!-- #SECONDARY, la sidebar -->
                            <div id="secondary" class="col-md-4">
                                <?php get_sidebar(); ?>
                            </div><!-- #secondary -->
<?php get_footer(); ?>
