<article <?php post_class();?>>
    <div class="date"><?php the_date('l, d \d\e F Y'); ?></div>
    <div class="post-row row">
        <div class="col-md-12 p-cont">
			<h2><?php the_title(); ?></h2>
        </div>
		<div class="col-md-6 col-md-offset-3">
            <div class="entry">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <?php the_content(); ?>
                    </div>
                </div>
            </div><!-- .entry -->
		</div><!-- .col-md-12 -->
    </div><!-- .post-row -->
</article>
