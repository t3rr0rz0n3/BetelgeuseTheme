<?php get_header(); ?>
	<div id="primary" class="content-area col-md-12">
		<main id="main">

			<header class="page-header">
				<h1 class="page-title entry-title">
					<?php single_cat_title( $prefix = '', $display = true ); ?>
				</h1>
			</header><!-- .page-header -->

			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

				<!-- Muestra el fichero content.php, donde carga los artículos -->
				<?php
					if (is_category('blog')) {
						get_template_part('content-archive-blog');
					} else {
						get_template_part('content-archive');
					}
				?>

			<?php endwhile; else: ?>
				<?php
					get_template_part( '404');
				?>

			<?php endif; ?>
			<nav class="navigation paging-navigation" role="navigation">
				<div class="nav-links">
					<?php custom_pagination(); ?>
				<div><!-- .nav-links -->
			</nav><!-- .navigation -->
		</main>
	</div><!-- #primary -->

<?php get_footer(); ?>
