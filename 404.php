<?php get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<header class="page-header">
				<h1 class="page-title"><?php _e( '¡Oh, no! 😮 ¡Maldición!', 'BetelgeuseTheme' ); ?></h1>
			</header><!-- .page-header -->

			<div class="post-inner-content error-content">
				<div class="page-content">
					<p class="error404 text-center">
						<?php echo ErroRandomFun(); ?>
					</p>
					<p class="h3error text-center"><?php _e( 'Por desgracia, esta página no existe.', 'BetelgeuseTheme') ?></p>
					<p class="h4error text-center"><?php _e( 'Te dejamos un buscador aquí para que vuelvas a intentarlo', 'BetelgeuseTheme' ); ?></p>

					<div class="row">
						<div class="col-md-6 col-md-offset-3">
							<?php get_search_form(); ?>
						</div>
					</div>

				</div><!-- .page-content -->
			</div><!-- .post-inner-content -->
		</main><!-- #main -->
	</div>
<?php get_footer(); ?>
