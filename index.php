<?php get_header(); ?>
                            <!-- #PRIMARY, las entradas, la entrada, la pagina, etc.. -->
                            <div id="primary" class="home col-md-8">
                                <main id="main">
                                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                                        <?php get_template_part( 'content'); ?>

                                    <?php endwhile; else: ?>

                                        <div class="no-articles">
                                            <h1>
                                                <?php _e('Aún no hay artículos para cargar', 'BetelgeuseTheme'); ?>
                                            </h1>
                                        </div>

                                    <?php endif; ?>
                                    <nav class="navigation paging-navigation" role="navigation">
                                        <div class="nav-links">
                                            <?php custom_pagination(); ?>
                                        <div><!-- .nav-links -->
                                    </nav><!-- .navigation -->
                                </main>
                            </div><!-- #primary -->

                            <div id="secondary" class="col-md-4">
                                <main id="sidebar">
                                    <?php get_sidebar(); ?>
                                </main>
                            </div>
                            <!-- Sidebar not present in index.php-->

<?php get_footer(); ?>
