<?php $search_terms = htmlspecialchars( $_GET["s"] ); ?>
<form role="form" action="<?php bloginfo('url'); ?>/" id="searchform" method="get">
    <label for="s" class="sr-only"><?php _e('Buscar', 'BetelgeuseTheme') ?></label>
    <div class="input-group">
        <input type="text" class="form-control" id="s" name="s" placeholder="Buscar" <?php if ( $search_terms !== '' ) { echo ' value="' . $search_terms . '"'; } ?> />
        <span class="input-group-btn">
            <button type="submit" type="button" class="btn btn-primary"><span class="glyphicon glyphicon-search"></span></button>
        </span>
    </div> <!-- .input-group -->
</form>
