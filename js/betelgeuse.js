// popover
$('#cookie').popover()

// Close cookies
$(document).ready(function(){
    var nombreCookie="PlaneetLibre-cookie";

    if(getCookie(nombreCookie)==""){
        $('.cookies').removeClass("oculto");
        $('.close-capa').dblclick(function(){
            $(this).fadeOut(500);
            createCookie(nombreCookie,1,365);
        });
    };
});

function createCookie(name,value,days){
    if(days){
        var date=new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires="; expires="+date.toGMTString();
    }else
        var expires="";
        document.cookie=name+"="+value+expires+"; path=/";
}

function readCookie(name){
    var nameEQ=name+"=";
    var ca=document.cookie.split(';');

    for(var i=0;i<ca.length;i++){
        var c=ca[i];

        while (c.charAt(0) == ' ') {
            c = c.substring(1,c.length);
        }

        if (c.indexOf(nameEQ)==0){
            return c.substring(nameEQ.length,c.length);
        }
    }
    return null;
}

function eraseCookie(name){
    createCookie(name,"",-1);
}

function getCookie(cname){
    var name=cname+"=";
    var ca=document.cookie.split(';');

    for(var i=0;i<ca.length;i++){
        var c=ca[i];

        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }

        if (c.indexOf(name) == 0) {
            return c.substring(name.length,c.length);
        }
    }
    return"";
}

$('#myModal').on('shown.bs.modal', function () {
    $('#myInput').focus()
})

function gsShare($id) {

    var gs_node = document.getElementById('gs-node-' + $id).value;
    var gs_msg = document.getElementById('gs-msg-' + $id).value;
    var gs_url = document.getElementById('gs-url-' + $id).value;

    var httpProtocol = gs_node.substring(0,5);
    if (gs_node == "") {
        alert("¡No has escrito ningún nodo!");
        $(gs_node).focus();
    } else {
        if (httpProtocol == 'https') {
            var urlNewWin = gs_node + '/notice/new?status_textarea=' + gs_msg + " " + gs_url + " " + '#PlanetLibre';
            OpenInNewTab(urlNewWin);
        } else {
            var urlNewWin = 'https://' + gs_node + '/notice/new?status_textarea=' + gs_msg + " " + gs_url + " " + '#PlanetLibre';
            OpenInNewTab(urlNewWin);
        }
    }
}

function dpShare($id) {

    var dp_node = document.getElementById('dp-node-' + $id).value;
    var dp_msg = document.getElementById('dp-msg-' + $id).value;
    var dp_url = document.getElementById('dp-url-' + $id).value;

    var httpProtocol = dp_node.substring(0,5);
    if (dp_node == "") {
        alert("¡No has escrito ningún nodo!")
    } else {
        if (httpProtocol == 'https') {
            var urlNewWin = dp_node + '/bookmarklet?url=' + dp_url + '&title=' + dp_msg + '&jump=doclose';
            OpenInNewTab(urlNewWin);
        } else {
            var urlNewWin = 'https://' + dp_node + '/bookmarklet?url=' + dp_url + '&title=' + dp_msg + '&jump=doclose';
            OpenInNewTab(urlNewWin);
        }
    }
}

function OpenInNewTab($url) {
    var win = window.open($url, '_blank');
    win.focus();
}
