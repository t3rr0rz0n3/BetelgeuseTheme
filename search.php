<?php get_header(); ?>
	<div id="primary" class="content-area col-md-12 search">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title entry-title">
					<?php printf( __( 'Resultados para: %s', 'BetelgeuseTheme' ), '<span>' . get_search_query() . '</span>' ); ?>
				</h1>
			</header><!-- .page-header -->

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content-search', 'search' ); ?>

			<?php endwhile; ?>

		<?php else : ?>

			<?php get_template_part( '404' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
